<?php

/**
 * Returns the importmap for this application.
 *
 * - "path" is a path inside the asset mapper system. Use the
 *     "debug:asset-map" command to see the full list of paths.
 *
 * - "entrypoint" (JavaScript only) set to true for any module that will
 *     be used as an "entrypoint" (and passed to the importmap() Twig function).
 *
 * The "importmap:require" command can be used to add new entries to this file.
 */
return [
    'app' => [
        'path' => './assets/app.js',
        'entrypoint' => true,
    ],
    '@symfony/stimulus-bundle' => [
        'path' => './vendor/symfony/stimulus-bundle/assets/dist/loader.js',
    ],
    'axios' => [
        'version' => '1.6.8',
    ],
    '@hotwired/stimulus' => [
        'version' => '3.2.2',
    ],
    'toastify-js' => [
        'version' => '1.12.0',
    ],
    'notyf' => [
        'version' => '3.10.0',
    ],
    'lucide' => [
        'version' => '0.390.0',
    ],
    'tippy.js' => [
        'version' => '6.3.7',
    ],
    '@popperjs/core' => [
        'version' => '2.11.8',
    ],
    'simplebar' => [
        'version' => '6.2.6',
    ],
    'can-use-dom' => [
        'version' => '0.1.0',
    ],
    'simplebar-core' => [
        'version' => '1.2.5',
    ],
    'simplebar/dist/simplebar.min.css' => [
        'version' => '6.2.6',
        'type' => 'css',
    ],
    'lodash-es' => [
        'version' => '4.17.21',
    ],
    'simplebar-core/dist/simplebar.min.css' => [
        'version' => '1.2.5',
        'type' => 'css',
    ],
    'prismjs' => [
        'version' => '1.29.0',
    ],
    'prismjs/themes/prism.min.css' => [
        'version' => '1.29.0',
        'type' => 'css',
    ],
    'apexcharts' => [
        'version' => '3.49.1',
    ],
    '@symfony/ux-live-component' => [
        'path' => './vendor/symfony/ux-live-component/assets/dist/live_controller.js',
    ],
];
