<?php

namespace App\Command;

use App\Context\Wordpress\Woocommerce\UseCase\CreateCategoriesOnWoocommerce;
use App\Context\Wordpress\Woocommerce\UseCase\CreateProductsOnWoocommerce;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Insert les éléments dans la base de donnée woocommerce
 * afin de permettre la synchro avec wordpress.
 */
#[AsCommand('app:woocommerce:data:seed')]
class SeedWoocommerceDataCommand extends Command
{
    public function __construct(private readonly CreateCategoriesOnWoocommerce $createCategoriesOnWoocommerce, private readonly CreateProductsOnWoocommerce $createProductsOnWoocommerce)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $symfonyStyle = new SymfonyStyle($input, $output);
        $this->createCategoriesOnWoocommerce->create($symfonyStyle);
        // $this->createProductsOnWoocommerce->create($symfonyStyle);

        return Command::SUCCESS;
    }
}
