<?php

namespace App\Command;

use App\Context\Order\UseCase\UpdateOrderStatus;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('app:orders:update-status')]
class UpdateOrderStatusCommand extends Command
{
    public function __construct(private readonly UpdateOrderStatus $orderStatus)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->orderStatus->updateOrders();

        return Command::SUCCESS;
    }
}
