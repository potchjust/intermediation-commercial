<?php

namespace App\EventSubscriber;

use App\Context\Auth\Entity\Supplier;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class JWTSubscriber implements EventSubscriberInterface
{
    public function onLexikJwtAuthenticationOnJwtCreated($event): void
    {
        // ...
        $data = $event->getData();
        $user = $event->getUser();
        if ($user instanceof Supplier) {
            $data['email'] = $user->getEmail();
            $data['name'] = $user->getName();
            $data['subDomain'] = $user->getSubDomain();
        }
        $event->setData($data);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'lexik_jwt_authentication.on_jwt_created' => 'onLexikJwtAuthenticationOnJwtCreated',
        ];
    }
}
