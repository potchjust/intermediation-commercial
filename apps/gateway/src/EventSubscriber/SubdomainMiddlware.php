<?php

namespace App\EventSubscriber;

use App\Context\Auth\Entity\Supplier;
use App\Context\Catalog\DTO\ProductDTO;
use App\Context\Catalog\Entity\Product;
use App\Context\Catalog\Repository\SupplierRepository;
use Doctrine\Common\EventSubscriber;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SubdomainMiddlware implements EventSubscriberInterface
{

    public function __construct(private readonly SupplierRepository $supplierRepository,
                                private readonly UrlGeneratorInterface $urlGenerator)
    {

    }

    /**
     * @return array{KernelEvents.REQUEST: string}
     */
    public static function getSubscribedEvents(): array
    {
       return [
           KernelEvents::REQUEST => 'onKernelRequest',
       ];
    }

    public function onKernelRequest(RequestEvent $event): void
    {
       $request = $event->getRequest();
       $host = $request->getHost();
       $pathInfo = $request->getPathInfo();

       $subdomains = explode('.', $host);

       if (count($subdomains) > 1) {
           $contextSubdomain = $subdomains[0];
           if ($contextSubdomain !== 'www' && $contextSubdomain !== 'example') {
               // Check in the datanase who have this custom domain
               $currentVendor = $this->supplierRepository->findOneBy(['subdomain' => $contextSubdomain]);
               // get the products related to this vendor
               if ($currentVendor) {
                   // Check if the current request is already for the target URL
                   if ($request->getRequestUri() !== '/home') {
                       // Redirect to the home page based on the subdomain
                       $request->attributes->add(['subdomain' => $contextSubdomain]);
                   }
               }
           }
       }
    }

}
