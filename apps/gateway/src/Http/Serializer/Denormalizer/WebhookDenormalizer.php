<?php

namespace App\Http\Serializer\Denormalizer;

use App\Context\Whatsapp\Dto\Webhook\WhatsappWebhook;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

final class WebhookDenormalizer implements DenormalizerInterface
{
    public function denormalize(mixed $data, string $type, ?string $format = null, array $context = []): mixed
    {
        return WhatsappWebhook::generateObject($data);
    }

    public function supportsDenormalization(mixed $data, string $type, ?string $format = null, array $context = []): bool
    {
        return 'App\Context\Whatsapp\Dto\Webhook\WhatsappWebhook' === $type;
    }

    public function getSupportedTypes(?string $format): array
    {
        return [
            WhatsappWebhook::class => true,
        ];
    }
}
