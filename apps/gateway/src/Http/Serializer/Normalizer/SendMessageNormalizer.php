<?php

namespace App\Http\Serializer\Normalizer;

use App\Context\Whatsapp\Dto\Messages\Interactive\WhatsappInteractiveMessage;
use App\Context\Whatsapp\Dto\Messages\Interactive\WhatsappInteractiveMessageAction;
use App\Context\Whatsapp\Dto\Messages\Interactive\WhatsappInteractiveMessageButton;
use App\Context\Whatsapp\Dto\Messages\Template\WhatsappMessageTemplate;
use App\Context\Whatsapp\Dto\Messages\Template\WhatsappMessageTemplateComponents;
use App\Context\Whatsapp\Dto\Messages\Template\WhatsappMessageTemplateComponentsParameters;
use App\Context\Whatsapp\Dto\Messages\Text\WhatsappTextMessage;
use App\Context\Whatsapp\Dto\Messages\WhatsappMessage;
use App\Context\Whatsapp\Enums\WhatsappMessageComponentsSubType;
use App\Context\Whatsapp\Enums\WhatsappMessageComponentsType;
use App\Context\Whatsapp\Enums\WhatsappMessageParametersType;
use App\Context\Whatsapp\Enums\WhatsappMessageType;
use App\Context\Whatsapp\Interface\SendMessageBodyFormater;
use Symfony\Component\Serializer\Exception\UnsupportedException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class SendMessageNormalizer implements NormalizerInterface, SendMessageBodyFormater
{
    public function normalize(mixed $object, ?string $format = null, array $context = []): array
    {
        $data = get_class($object);
        $txt = sprintf('There are million bicycles in %s.', $data);
        $type = match (true) {
            $object instanceof WhatsappMessageTemplate => WhatsappMessageType::TEMPLATE->value,
            $object instanceof WhatsappInteractiveMessage => WhatsappMessageType::INTERACTIVE->value,
            $object instanceof WhatsappTextMessage => WhatsappMessageType::TEXT->value,
            default => throw new UnsupportedException($txt)
        };
        $data = $this->reformatMessage($object);
        switch ($type) {
            case WhatsappMessageType::from($type)->value == WhatsappMessageType::TEMPLATE->value:
                assert($object instanceof WhatsappMessageTemplate);
                $data['template'] = $this->reformatMessageTemplate($object);
                $data['template']['components'] = $this->reformatMessageTemplateComponents($object->getComponents());
                break;
            case WhatsappMessageType::from($type)->value == WhatsappMessageType::INTERACTIVE->value:
                assert($object instanceof WhatsappInteractiveMessage);
                $data['recipient_type'] = 'individual';
                $data['interactive'] = $this->reformatInteractiveMessage($object);
                $data['interactive']['action'] = $this->reformatInteractiveAction($object->getWhatsappInteractiveMessageAction());
                break;
            case WhatsappMessageType::from($type)->value == WhatsappMessageType::TEXT->value:
                assert($object instanceof WhatsappTextMessage);
                $data['text'] = $object->getContent();
                break;
            default:
                throw new UnsupportedException('The current type is not supported');
        }

        return $data;
    }

    public function supportsNormalization(mixed $data, ?string $format = null, array $context = []): bool
    {
        return $data instanceof WhatsappMessage;
    }

    public function getSupportedTypes(?string $format): array
    {
        return [
            WhatsappMessage::class => true,
        ];
    }

    public function reformatMessageTemplateComponentParameters(array $whatsappMessageTemplateComponentsParameters): array
    {
        $parameters = [];
        foreach ($whatsappMessageTemplateComponentsParameters as $whatsappMessageTemplateComponentsParameter) {
            assert($whatsappMessageTemplateComponentsParameter instanceof WhatsappMessageTemplateComponentsParameters);
            $parameter = [
                'type' => WhatsappMessageParametersType::from($whatsappMessageTemplateComponentsParameter->getType())->value,
            ];
            switch ($whatsappMessageTemplateComponentsParameter->getType()) {
                case WhatsappMessageParametersType::IMAGE->value:
                    $parameter['image'] = $whatsappMessageTemplateComponentsParameter->getAction();
                    break;
                case WhatsappMessageParametersType::TEXT->value:
                    $parameter['text'] = $whatsappMessageTemplateComponentsParameter->getText();
                    break;
                case WhatsappMessageParametersType::PAYLOAD->value:
                    $parameter['payload'] = $whatsappMessageTemplateComponentsParameter->getAction()['payload'];
                    break;
                case WhatsappMessageParametersType::ACTION->value:
                    $parameter['action'] = $whatsappMessageTemplateComponentsParameter->getAction();
                    break;
            }
            $parameters[] = $parameter;
        }

        return $parameters;
    }

    public function reformatMessageTemplateComponents(array $messageTemplateComponents): array
    {
        $components = [];
        foreach ($messageTemplateComponents as $messageTemplateComponent) {
            assert($messageTemplateComponent instanceof WhatsappMessageTemplateComponents);
            $singleComponent = [
                'type' => WhatsappMessageComponentsType::from($messageTemplateComponent->getType())->value,
            ];
            if (null !== $messageTemplateComponent->getSubType()) {
                $singleComponent['sub_type'] = WhatsappMessageComponentsSubType::from($messageTemplateComponent->getSubType())->value;
            }
            if (null !== $messageTemplateComponent->getIndex()) {
                $singleComponent['index'] = $messageTemplateComponent->getIndex();
            }
            $singleComponent['parameters'] = [...$this->reformatMessageTemplateComponentParameters($messageTemplateComponent->getParameters())];
            $components[] = $singleComponent;
        }

        return $components;
    }

    public function reformatMessageTemplate(WhatsappMessageTemplate $messageTemplate): array
    {
        return [
            'name' => $messageTemplate->getName(),
            ...$messageTemplate->getLangage(),
        ];
    }

    public function reformatInteractiveMessage(WhatsappInteractiveMessage $interactiveMessage): array
    {
        return [
            'type' => $interactiveMessage->getInteractiveButtonType(),
            ...$interactiveMessage->getBodyText(),
        ];
    }

    public function reformatInteractiveAction(WhatsappInteractiveMessageAction $interactiveMessageAction): array
    {
        $interactiveMessageAction_ = $interactiveMessageAction->getActionsButtons()[0];
        assert($interactiveMessageAction_ instanceof WhatsappInteractiveMessageButton);
        $data = [
            'type' => $interactiveMessageAction_->getType(),
            ...$interactiveMessageAction_->getParams(),
        ];
        $buttons['buttons'][] = $data;

        return $buttons;
    }

    public function reformatMessage(WhatsappMessage $whatsappMessage): array
    {
        return [
            'messaging_product' => $whatsappMessage->getMessagingProduct(),
            'to' => $whatsappMessage->getTo(),
            'type' => WhatsappMessageType::from($whatsappMessage->getType())->value,
        ];
    }
}
