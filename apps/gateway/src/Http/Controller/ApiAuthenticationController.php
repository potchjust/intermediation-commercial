<?php

namespace App\Http\Controller;

use App\Context\Auth\Entity\Supplier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

class ApiAuthenticationController extends AbstractController
{
    #[Route(path: '/auth/login', name: 'api_login', )]
    public function login(#[CurrentUser] ?Supplier $loggedUser): Response
    {
        if (null === $loggedUser) {
            return $this->json(['message' => 'missing credentials'], Response::HTTP_UNAUTHORIZED);
        }

        // retourner un token
        $supplier = $this->getUser();
        assert($supplier instanceof Supplier);

        return new JsonResponse([
            'id' => $supplier->getId(),
            'email' => $supplier->getEmail(),
            'name' => $supplier->getName(),
            'subDomain' => $supplier->getSubdomain(),
        ]);
    }

    #[Route(path: '/logout', name: 'app_logout')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
