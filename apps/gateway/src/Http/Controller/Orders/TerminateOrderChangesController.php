<?php

namespace App\Http\Controller\Orders;

use App\Context\Order\Entity\OrderDetails;
use App\Context\Order\Entity\SubOrder;
use App\Context\Order\Enums\OrderDetailsStatus;
use App\Context\Order\Repository\SubOrderRepository;
use App\Context\Order\UseCase\TerminateOrderChanges;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Uid\UuidV7;

class TerminateOrderChangesController extends AbstractController
{
    public function __construct(private TerminateOrderChanges $terminateOrderChanges, private SubOrderRepository $subOrders)
    {
    }

    #[Route(path: '/order/{orderId}/supplier/terminate', name: 'sub_orders_terminate', methods: ['PUT'])]
    public function terminate(string $orderId): JsonResponse
    {
        $subOrder = $this->subOrders->findOneBy(['id' => UuidV7::fromString(trim($orderId))]);
        assert($subOrder instanceof SubOrder);
        $hasItemsPendingAttention = array_filter($subOrder->getOrderDetails()->toArray(), function (OrderDetails $orderDetails) {
            return $orderDetails->getStatus() === OrderDetailsStatus::PENDING->name;
        });
        $message = 'DEFAULT';
        if (0 === count($hasItemsPendingAttention)) {
            $message = 'ALL_ITEMS_CHECKED';
            $this->terminateOrderChanges->terminate($subOrder);
        } else {
            $message = 'ITEMS_PENDING_ATTENTION';
        }

        return new JsonResponse([
            'message' => $message,
        ]);
    }
}
