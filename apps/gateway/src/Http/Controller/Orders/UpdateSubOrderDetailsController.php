<?php

namespace App\Http\Controller\Orders;

use App\Context\Auth\Entity\Supplier;
use App\Context\Order\Entity\OrderDetails;
use App\Context\Order\Enums\OrderDetailsStatus;
use App\Context\Order\Repository\OrderDetailsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Uid\UuidV7;

class UpdateSubOrderDetailsController extends AbstractController
{
    public function __construct(private readonly OrderDetailsRepository $orderDetailsRepository, private readonly EntityManagerInterface $entityManager)
    {
    }

    #[Route(path: '/sub-orders/{subOrderId}/details/{detailsId}/validate', name: 'sub_orders_details_validate', )]
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    public function validateOrderItem(#[CurrentUser] $supplier, string $subOrderId, string $detailsId)
    {
        assert($supplier instanceof Supplier);
        $orderDetails = $this->orderDetailsRepository->findOneBy(['subOrder' => UuidV7::fromString(trim($subOrderId)), 'id' => UuidV7::fromString(trim($detailsId))]);
        assert($orderDetails instanceof OrderDetails);
        $orderDetails->setStatus(OrderDetailsStatus::VALIDATED->name);
        $this->entityManager->persist($orderDetails);
        $this->entityManager->flush();

        // $eventManager = $this->entityManager->getEventManager();
        // $eventManager->dispatchEvent(Events::preUpdate, new LifecycleEventArgs($orderDetails, $this->entityManager));

        // dd($eventManager);
        return new JsonResponse($orderDetails);
    }

    #[Route(path: '/sub-orders/{subOrderId}/details/{detailsId}/cancel', name: 'sub_orders_details_cancel', )]
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    public function cancelOrderItem(#[CurrentUser] $supplier, string $subOrderId, string $detailsId)
    {
        assert($supplier instanceof Supplier);
        $orderDetails = $this->orderDetailsRepository->findOneBy(['subOrder' => UuidV7::fromString(trim($subOrderId)), 'id' => UuidV7::fromString(trim($detailsId))]);
        assert($orderDetails instanceof OrderDetails);
        $orderDetails->setStatus(OrderDetailsStatus::NOT_AVAILABLE->name);
        $this->entityManager->persist($orderDetails);
        $this->entityManager->flush();

        return new JsonResponse($orderDetails);
    }
}
