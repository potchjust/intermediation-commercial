<?php

namespace App\Http\Controller\Orders;

use App\Context\Auth\Entity\Supplier;
use App\Context\Catalog\Form\OrderDetailsType;
use App\Context\Catalog\Repository\ProductRepository;
use App\Context\Order\DTO\SupplierOrdersDTO;
use App\Context\Order\Entity\Order;
use App\Context\Order\Entity\SubOrder;
use App\Context\Order\Enums\OrderStatus;
use App\Context\Order\Enums\SubOrderStatus;
use App\Context\Order\Repository\SubOrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class GetSubOrderDetailsController extends AbstractController
{
    public function __construct(private readonly SubOrderRepository $subOrderRepository, private readonly EntityManagerInterface $entityManager, private ProductRepository $productRepository)
    {
    }

    #[Route(path: '/sub-orders/{subOrderId}/details', name: 'sub_orders_details', )]
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    public function listSubOrdersPerSupplier(#[CurrentUser] $supplier, string $subOrderId)
    {
        assert($supplier instanceof Supplier);

        $subOrder = $this->subOrderRepository->findOneBy(['id' => $subOrderId]);
        assert($subOrder instanceof SubOrder);
        $order = $subOrder->getOrder();
        assert($order instanceof Order);
        if ($order->getStatus() === OrderStatus::NEW->name) {
            $order->setStatus(OrderStatus::PROCESSING->name);
            $this->entityManager->persist($order);
            $this->entityManager->flush();
        }

        if ($subOrder->getStatus() === SubOrderStatus::NEW->name) {
            $subOrder->setStatus(SubOrderStatus::PROCESSING->name);
            $this->entityManager->persist($subOrder);
            $this->entityManager->flush();
        }

        $serialized = new SupplierOrdersDTO($subOrder, $this->productRepository);
        // create the form
        $forms = [];
        foreach ($serialized->getDetails() as $orderDetail) {
            $orderDetailForm = $this->createForm(OrderDetailsType::class, $orderDetail);
            $forms[] = $orderDetailForm->createView();
        }



        return $this->render('dashboard/orders/_details.html.twig', [
            'order' => $serialized,
            'forms' => $forms,
        ]);
    }
}
