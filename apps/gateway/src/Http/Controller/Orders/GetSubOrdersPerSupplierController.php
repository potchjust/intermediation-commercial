<?php

namespace App\Http\Controller\Orders;

use App\Context\Auth\Entity\Supplier;
use App\Context\Catalog\Repository\ProductRepository;
use App\Context\Order\DTO\SupplierOrdersDTO;
use App\Context\Order\Entity\SubOrder;
use App\Context\Order\Enums\SubOrderStatus;
use App\Context\Order\Repository\SubOrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class GetSubOrdersPerSupplierController extends AbstractController
{
    public function __construct(private readonly SubOrderRepository $subOrderRepository, private readonly ProductRepository $productRepository)
    {
    }

    #[Route(path: '/sub-orders', name: 'app_orders')]
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    public function listSubOrdersPerSupplier(#[CurrentUser] $supplier)
    {
        assert($supplier instanceof Supplier);
        $subOrders = $this->subOrderRepository->getBySupplier($supplier);
        $subOrders = array_map(fn (SubOrder $subOrder) => new SupplierOrdersDTO($subOrder, $this->productRepository), $subOrders);


        $subOrderCancelled = array_filter($subOrders, function (SupplierOrdersDTO $subOrder) {
            return $subOrder->getStatus()['name'] === SubOrderStatus::HAS_BEEN_CANCELLED_BY_CUSTOMER->name;
        });
        $subOrderProcessing = array_filter($subOrders, function (SupplierOrdersDTO $subOrder) {
            return $subOrder->getStatus()['name'] === SubOrderStatus::PROCESSING->name;
        });
        $subOrderCompleted = array_filter($subOrders, function (SupplierOrdersDTO $subOrder) {
            return $subOrder->getStatus()['name'] === SubOrderStatus::VALIDATED->name;
        });
        $subOrderNew = array_filter($subOrders, function (SupplierOrdersDTO $subOrder) {
            return $subOrder->getStatus()['name'] === SubOrderStatus::NEW->value;
        });

        $count = [
            'total' => count($subOrders),
            'new' => count($subOrderNew),
            'processing' => count($subOrderProcessing),
            'completed' => count($subOrderCompleted),
            'cancelled' => count($subOrderCancelled),
        ];


        return $this->render('dashboard/orders/index.html.twig', [
            'counts' => $count,
            'orders' => $subOrders]);
    }
}
