<?php

namespace App\Http\Controller\Catalog;

use App\Context\Auth\Entity\Supplier;
use App\Context\Auth\Entity\User;
use App\Context\Catalog\DTO\ProductDTO;
use App\Context\Catalog\Entity\Product;
use App\Context\Catalog\Form\ProductType;
use App\Context\Catalog\Repository\ProductRepository;
use App\Context\Catalog\UseCase\Products\CreateProduct;
use App\Context\Catalog\UseCase\Products\UpdateProduct;
use App\Infrastructure\Wordpress\Woocommerce\Request\WoocommerceSendDeleteRequest;
use App\Infrastructure\Wordpress\Woocommerce\WooCommerceApi;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ProductsController extends AbstractController
{
    public function __construct(
        private readonly ProductRepository $productRepository,
        private CreateProduct $createProduct,
        private UpdateProduct $updateProduct,
        private WooCommerceApi $woocommerceApi,
        private EntityManagerInterface $entityManager,
    ) {
    }

    #[Route(path: '/products', name: 'app_suppliers_products', methods: ['GET'])]
    public function index(Request $request): Response
    {
        // if the user is a supplier we display his data
        // if the user is an admin we display all the products
        $currentUser = $this->getUser();
        $products = [];
        // get the current item and get his type
        if ($currentUser) {
            assert($currentUser instanceof Supplier);
            $products = array_map(static fn (Product $product) => new ProductDTO($product), $currentUser->getProducts()->toArray());
        } else {
            $products = $this->productRepository->findAll();
        }

        return $this->render('dashboard/products/index.html.twig', [
            'products' => $products,
        ]);
    }

    #[Route(path: '/products/new', name: 'app_suppliers_products_new', methods: ['GET'])]
    public function new(Request $request): Response
    {
        // create the form
        $product = new Product();
        $addNewProductForm = $this->createForm(ProductType::class, $product);

        return $this->render('dashboard/products/new.html.twig', [
            'form' => $addNewProductForm->createView(),
        ]);
    }

    #[Route(path: '/products/new/submit', name: 'app_suppliers_products_new_submit', methods: ['POST'])]
    public function save(Request $request): Response
    {
        // create the form
        $product = new Product();
        $addNewProductForm = $this->createForm(ProductType::class, $product);
        $addNewProductForm->handleRequest($request);
        if ($addNewProductForm->isSubmitted() && $addNewProductForm->isValid()) {
            $productData = $addNewProductForm->getData();
            $currentSupplier = $this->getUser();
            assert($currentSupplier instanceof Supplier);
            $productData->setSupplier($currentSupplier);
            $this->createProduct->execute($productData);
            $this->addFlash(
                'success',
                'Le produit a été enregistré avec succès'
            );

            return $this->redirectToRoute('app_suppliers_products');
        }

        return $this->redirectToRoute('app_suppliers_products');
    }

    #[Route(path: '/products/{id}', name: 'app_suppliers_products_view_update', methods: ['GET|POST'])]
    public function editProduct(Product $product, Request $request)
    {
        $editProductForm = $this->createForm(ProductType::class, $product);
        $editProductForm->handleRequest($request);
        if ($editProductForm->isSubmitted() && $editProductForm->isValid()) {
            $this->updateProduct->execute($product);
            $this->addFlash(
                'success',
                'Le produit a été mise à jour avec succès'
            );

            return $this->redirectToRoute('app_suppliers_products');
        }

        return $this->render('dashboard/products/edit.html.twig', [
            'form' => $editProductForm->createView(),
            'product' => $product,
        ]);
    }

    #[Route(path: '/products/{id}/delete', name: 'app_suppliers_products_delete', methods: ['DELETE'])]
    public function deleteProduct(Product $product)
    {
        $deleteProductRequest = new WoocommerceSendDeleteRequest("products/{$product->getWoocommerceId()}");
        $requestItem['force'] = true;
        $deleteProductRequest->addParameter($requestItem);
        $response = json_decode($this->woocommerceApi->send($deleteProductRequest), true);
        if ($response['id']) {
            $this->addFlash(
                'success',
                'Le produit a bien été supprimé avec succès'
            );
            $this->entityManager->remove($product);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('app_suppliers_products');
    }
}
