<?php

namespace App\Http\Controller\Catalog;

use App\Context\Auth\Entity\Supplier;
use App\Context\Auth\Entity\User;
use App\Context\Auth\Repository\UsersRepository;
use App\Context\Catalog\Entity\Catalog;
use App\Context\Catalog\Form\CatalogType;
use App\Context\Catalog\Repository\CatalogRepository;
use App\Context\Catalog\Repository\SupplierRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class CatalogsController extends AbstractController
{
    public function __construct(private readonly UsersRepository $usersRepository,
        private readonly SupplierRepository $supplierRepository,
        private readonly CatalogRepository $catalogRepository, )
    {
    }

    #[Route(path: '/catalogs', name: 'app_catalogs_supplier', methods: ['GET|POST'])]
    public function index(Request $request): Response
    {
        // if the user is a supplier we display his data
        // if the user is an admin we display all the catalogs
        $currentUser = $this->getUser();
        $catalogs = $this->catalogRepository->findAll();
        $form = null;
        // get the current item and get his type
        if ($currentUser) {
            $supplierOrAdmin = $this->usersRepository->findOneBy(['id' => $currentUser->getId()]);
            // check the type
            if ($supplierOrAdmin) {
                assert($supplierOrAdmin instanceof User);
                // get the supplier
                $supplier = $this->supplierRepository->findOneBy(['id' => $supplierOrAdmin->getId()]);
                if ($supplier) {
                    $catalogs = $this->catalogRepository->findAllBySupplier($supplier);
                    if (0 == count($catalogs)) {
                        // we can create t the catalog
                        $catalog = new Catalog();
                        $catalog->setSupplier($supplierOrAdmin);
                        $catalog->setLabel('Test form');

                        $form = $this->createForm(CatalogType::class, $catalog);
                    }
                } else {
                    $catalogs = $this->catalogRepository->findAll();
                }
            }
        }

        return $this->render('dashboard/catalogs/index.html.twig', [
            'catalogs' => $catalogs,
            'form' => $form,
        ]);
    }
}
