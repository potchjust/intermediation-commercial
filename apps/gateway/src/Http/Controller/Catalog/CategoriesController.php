<?php

namespace App\Http\Controller\Catalog;

use App\Context\Auth\Entity\Supplier;
use App\Context\Catalog\DTO\CategoryDTO;
use App\Context\Catalog\Entity\Category;
use App\Context\Catalog\Form\CategoryType;
use App\Context\Catalog\Repository\CategoryRepository;
use App\Context\Catalog\UseCase\Categories\CreateCategories;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

class CategoriesController extends AbstractController
{
    public function __construct(private CategoryRepository $categoryRepository, private EntityManagerInterface $entityManager, private CreateCategories $createCategories)
    {
    }

    #[Route(path: '/categories', name: 'app_categories_list', methods: ['GET|POST'])]
    public function list(Request $request)
    {
        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category);
        if (Request::METHOD_POST === $request->getMethod()) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $category = $form->getData();
                $this->entityManager->persist($category);
                $this->createCategories->createCategoriesOnWooCommerce($category);
                $this->entityManager->flush();
            }
        }
        // fetch all the categories link to the current supplier
        $currentUser = $this->getUser();
        $categories = [];
        if ($currentUser instanceof Supplier) {
            $categories = []; // do on sql request
        } else {
            $categories = $this->categoryRepository->findAll();
        }
        $categories = array_map(static function (Category $category) {
            return new CategoryDTO($category->getLabel(), $category->getDescription(), $category->getProducts()->count(), $category->getCatalog()?->getLabel());
        }, $categories);

        return $this->render('dashboard/catalogs/categories/list.html.twig', [
            'form' => $form->createView(),
            'categories' => $categories,
        ]);
    }
    /* #[Route(path: '/categories/create', name: 'app_categories_create', methods: ['POST'])]
     public function create(Request $request)
     {
         $task = new Category();
         $form = $this->createForm(CategoryType::class, $task);
         dd($request->getPayload()->all());
     }*/
}
