<?php

namespace App\Http\Controller;

use App\Context\Auth\Entity\Customer;
use App\Context\Auth\UseCase\Customer\CheckCustomer;
use App\Context\Catalog\Repository\CustomerRepository;
use App\Context\Whatsapp\Dto\Webhook\WhatsappWebhook;
use App\Context\Whatsapp\Dto\Webhook\WhatsappWebhookMessages;
use App\Context\Whatsapp\Enums\WhatsappWebhookMessageTypes;
use App\Context\Whatsapp\UseCase\Whatsapp\Messages\SendMessageRequest;
use App\Context\Whatsapp\UseCase\Whatsapp\WhatsappButtonMessagesHandler;
use App\Context\Whatsapp\UseCase\Whatsapp\WhatsappOrderMessagesHandler;
use App\Context\Whatsapp\UseCase\Whatsapp\WhatsappTextMessagesHandler;
use App\Infrastructure\Whatsapp\WhatsappApi;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class WebhookController extends AbstractController
{
    public function __construct(private readonly WhatsappApi $whatsappApi,
        private readonly CheckCustomer $checkCustomer,
        private readonly WhatsappTextMessagesHandler $whatsappTextMessagesHandler,
        private readonly WhatsappOrderMessagesHandler $whatsappOrderMessagesHandler,
        private readonly WhatsappButtonMessagesHandler $whatsappButtonMessagesHandler,
        private CustomerRepository $customerRepository, )
    {
    }

    /**
     * @throws TransportExceptionInterface
     */
    #[Route('/webhook', methods: ['POST'])]
    public function webhook(Request $request, SerializerInterface $serialize)
    {
        $content = $request->getContent();
        $fileName = strval((new \DateTimeImmutable())->getTimestamp()).'.json';
        $webhook = $serialize->deserialize($content, WhatsappWebhook::class, 'json');
        assert($webhook instanceof WhatsappWebhook);
        if (count($webhook->getEntries()) > 0) {
            $entries = $webhook->getEntries();
            $changes = $entries[0]->getChanges();
            if (count($changes) > 0) {
                $webhookValues = $changes[0]->getWebhookValue();
                if (0 === count($webhookValues->getStatuses())) {
                    $contact = $webhookValues->getContacts()[0];
                    // if i get the contact i need to check if the current contact exists
                    // if he exists return the customer
                    $customer = $this->checkCustomer->check($contact);
                    // check if the user already exists
                    $webhookMessages = $webhookValues->getMessages();
                    if (count($webhookMessages) > 0) {
                        $webhookMessage = $webhookMessages[0];
                        assert($webhookMessage instanceof WhatsappWebhookMessages);
                        // create a cus
                        $webhookRequests = match ($webhookMessage->getType()) {
                            WhatsappWebhookMessageTypes::TEXT->value => $this->whatsappTextMessagesHandler->handle($webhookMessage, $customer) ,
                            WhatsappWebhookMessageTypes::ORDER->value => $this->whatsappOrderMessagesHandler->handleOrder($webhookMessage->getWhatsappWebhookOrder(), $customer),
                            WhatsappWebhookMessageTypes::BUTTON->value => $this->whatsappButtonMessagesHandler->handleButton($webhookMessage->getWhatsappWebhookButton(), $customer),
                            default => [] || null,
                        };
                        if (!$webhookRequests) {
                            throw new \UnexpectedValueException('The current type is not supported');
                        }
                        foreach ($webhookRequests as $webhookRequest) {
                            assert($webhookRequest instanceof SendMessageRequest);
                            $this->whatsappApi->send($webhookRequest);
                        }
                    }
                }
            }
        }

        return new JsonResponse('200');
    }

    #[Route('/webhook', methods: ['GET'])]
    public function getWebhook(Request $request)
    {
        $challenge = $request->query->get('hub_challenge');

        return new Response($challenge);
    }
}
