<?php

namespace App\Http\Controller\invoices;

use App\Context\Auth\Entity\Supplier;
use App\Context\Order\DTO\SupplierOrdersDTO;
use App\Context\Order\Entity\SubOrder;
use App\Context\Order\Repository\SubOrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class GetInvoicesListController extends AbstractController
{
    public function __construct(private readonly SubOrderRepository $subOrderRepository)
    {
    }

    #[Route(path: '/invoices', name: 'app_invoices')]
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    public function listSubOrdersPerSupplier(#[CurrentUser] $supplier)
    {
        assert($supplier instanceof Supplier);
        $subOrders = $this->subOrderRepository->getBySupplier($supplier);
        $subOrders = array_map(fn (SubOrder $subOrder) => new SupplierOrdersDTO($subOrder), $subOrders);

        return $this->render('dashboard/orders/index.html.twig', ['orders' => $subOrders]);
    }
}
