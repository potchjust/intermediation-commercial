<?php

namespace App\Http\Controller\invoices;

use App\Context\Invoice\UseCases\GetSingleOrder;
use App\Context\Order\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Uid\UuidV7;

class GetSingleInvoicesDetailsController extends AbstractController
{
    public function __construct(private readonly OrderRepository $orderRepository, private readonly GetSingleOrder $getSingleOrder)
    {
    }

    #[Route(path: '/invoices/order/{orderId}', name: 'app_invoices')]
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    public function listSubOrdersPerSupplier(string $orderId)
    {
        // order
        $order = $this->orderRepository->findOneBy(['id' => UuidV7::fromString(trim($orderId))]);
        $data = $this->getSingleOrder->get($order);

        return $this->render('dashboard/invoices/_details.html.twig', ['invoice' => $data]);
    }
}
