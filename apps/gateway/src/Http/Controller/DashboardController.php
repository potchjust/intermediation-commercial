<?php

namespace App\Http\Controller;

use App\Context\Auth\Entity\Supplier;
use App\Context\Catalog\Repository\ProductRepository;
use App\Context\Order\DTO\SupplierOrdersDTO;
use App\Context\Order\Entity\SubOrder;
use App\Context\Order\Enums\SubOrderStatus;
use App\Context\Order\Repository\SubOrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class DashboardController extends AbstractController
{
    public function __construct(
        private SubOrderRepository $subOrderRepository,
        private readonly ProductRepository $productRepository
    ) {
    }

    #[Route(path: '/admin', name: 'app_home', methods: ['GET'])]
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    public function show(Request $request, #[CurrentUser] $currentUser)
    {
        $currentRoles = $this->getUser()->getRoles();
        if (count($currentRoles) > 0) {
            $currentUserRole = $this->getUser()->getRoles()[0];
            if ('ROLE_SUPPLIER' === $currentUserRole) {
                // display the current admin informations
                // we will have the current user
                // the total revenue
                // total orders , total delivered, total cancelled
                assert($currentUser instanceof Supplier);
                $totalSales = 0;
                foreach ($currentUser->getPayments()->toArray() as $currentPayment) {
                    $totalSales += $currentPayment->getAmount();
                }
                // get the last orders
                $orders = array_map(function (SubOrder $subOrder) {
                    return new SupplierOrdersDTO($subOrder, $this->productRepository);
                }, $this->subOrderRepository->getSupplierOrders($currentUser, true));
                $cancelledOrders = $this->subOrderRepository->getSuppliersOrderByStatus($currentUser, [
                    SubOrderStatus::HAS_BEEN_CANCELLED_BY_CUSTOMER->name,
                ], true);
                $deliveredOrders = $this->subOrderRepository->getSuppliersOrderByStatus($currentUser, [
                    SubOrderStatus::VALIDATED->name,
                ], true);
                $data = [
                    'totalSales' => $totalSales,
                    'totalOrders' => $currentUser->getSubOrders()->count(),
                    'totalDelivered' => $deliveredOrders,
                    'cancelled' => $cancelledOrders,
                    'currentSupplier' => $currentUser->getName(),
                    'lastOrders' => count($orders),
                    'orders' => $orders,
                ];

                // create the DTO
                return $this->render('dashboard/index.html.twig', [
                    'data' => $data,
                ]);
            }
        }

        return $this->render('dashboard/index.html.twig');
    }
}
