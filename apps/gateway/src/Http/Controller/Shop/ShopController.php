<?php

namespace App\Http\Controller\Shop;

use App\Context\Auth\Entity\Supplier;
use App\Context\Catalog\DTO\ProductDTO;
use App\Context\Catalog\Entity\Product;
use App\Context\Catalog\Repository\SupplierRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ShopController extends AbstractController
{
    public function __construct(private readonly SupplierRepository $vendorRepository)
    {

    }

    #[Route(path: '/home', name: 'subdomain_route', host: '{subdomain}.intermediacom.localhost')]
    public function index(Request $request): Response
    {
        // get the current subdomain
        $subdomain = $request->attributes->get('subdomain');
        if ($subdomain !== null) {
            $currentVendor = $this->vendorRepository->findOneBy(['subdomain' => $subdomain]);
            if ($currentVendor === null) {
                return $this->redirectToRoute('subdomain_route');
            }
            // get the information and the data
            $currentVendorsProducts = array_map(static fn (Product $product) => new ProductDTO($product), $currentVendor->getProducts()->toArray());

            assert($currentVendor instanceof Supplier);
            $shopPageDataToReturn = [
                'vendor'=> [
                    'subdomain' => $currentVendor->getSubdomain(),
                    'name'=>$currentVendor->getName(),
                    'email'=>$currentVendor->getEmail(),
                    'phone'=>$currentVendor->getPhoneNumber(),
                    'address'=>$currentVendor->getAddress(),
                ],
                'products'=>$currentVendorsProducts
            ];
            return new JsonResponse($shopPageDataToReturn);
           // return $this->render('shop/index.html.twig', $shopPageDataToReturn);
        }
        return new Response("Hello from the subdomain: " . htmlspecialchars($subdomain));
    }
}
