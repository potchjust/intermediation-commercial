<?php

namespace App\Infrastructure\database\fixtures;

use App\Context\Auth\Entity\Supplier;
use App\Context\Catalog\Entity\Catalog;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\SerializerInterface;

class SuppliersFixtures extends Fixture
{
    public function __construct(private readonly KernelInterface $kernel, private readonly SerializerInterface $serializer,
        private readonly UserPasswordHasherInterface $userPasswordHasher,
        private readonly UrlGeneratorInterface $urlGenerator
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $suppliersJsonContentPath = $this->kernel->getProjectDir().'/etc/fixtures/suppliers.json';
        $suppliersJsonContent = file_get_contents($suppliersJsonContentPath);
        $suppliersData = $this->serializer->decode($suppliersJsonContent, 'json');
        foreach ($suppliersData as $supplierData) {
            $supplier = new Supplier();
            $supplier->setName($supplierData['nom']);
            $supplier->setAddress($supplierData['adresse']);
            $supplier->setEmail($supplierData['email']);
            $supplier->setPhoneNumber($supplierData['telephone']);
            $supplier->setPassword($this->userPasswordHasher->hashPassword($supplier, $supplierData['mot_de_passe']));
            $supplier->setSubdomain(strtolower($supplierData['nom']));
            // create the catalog
            $manager->persist($supplier);
        }
        $manager->flush();
    }
}
