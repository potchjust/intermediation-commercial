<?php

namespace App\Infrastructure\database\fixtures;

use App\Context\Catalog\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Serializer\SerializerInterface;

class CategoriesFixtures extends Fixture
{
    public function __construct(private readonly KernelInterface $kernel, private readonly SerializerInterface $serializer,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $categoriesJsonContentPath = $this->kernel->getProjectDir().'/etc/fixtures/categories.json';
        $categoriesJsonContent = file_get_contents($categoriesJsonContentPath);
        $categoriesAsArray = $this->serializer->decode($categoriesJsonContent, 'json');
        foreach ($categoriesAsArray as $categoryItem) {
            $category = new Category();
            $category->setLabel($categoryItem['name']);
            $category->setDescription($categoryItem['description']);
            $manager->persist($category);
        }
        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['group2'];
    }
}
