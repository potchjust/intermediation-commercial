<?php

namespace App\Infrastructure\database\fixtures;

use App\Context\Auth\Entity\Admin;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\SerializerInterface;

class AdminFixtures extends Fixture
{
    public function __construct(private readonly KernelInterface $kernel, private readonly SerializerInterface $serializer,
        private readonly UserPasswordHasherInterface $userPasswordHasher,
        private readonly UrlGeneratorInterface $urlGenerator
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $adminsJsonContentPath = $this->kernel->getProjectDir().'/etc/fixtures/admins.json';
        $adminsJsonContent = file_get_contents($adminsJsonContentPath);
        $adminData = $this->serializer->decode($adminsJsonContent, 'json');
        foreach ($adminData as $admin) {
            $admins = new Admin();
            $admins->setEmail($admin['email']);
            $admins->setPassword($this->userPasswordHasher->hashPassword($admins, $admin['mot_de_passe']));
            $manager->persist($admins);
        }
        $manager->flush();
    }
}
