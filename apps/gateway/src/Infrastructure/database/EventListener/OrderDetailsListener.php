<?php

namespace App\Infrastructure\database\EventListener;

use App\Context\Order\Entity\OrderDetails;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;

#[AsEntityListener(event: Events::postUpdate, entity: OrderDetails::class)]
class OrderDetailsListener
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    public function postUpdate(OrderDetails $conference, LifecycleEventArgs $event)
    {
        // get the current field that changes
        // $orderDetails = $event->getObject()->getOrderDetails();
    }
}
