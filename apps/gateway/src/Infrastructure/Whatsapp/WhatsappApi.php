<?php

namespace App\Infrastructure\Whatsapp;

use App\Context\Whatsapp\Dto\Response\WhatsappApiResponse;
use App\Context\Whatsapp\UseCase\Whatsapp\Messages\SendMessageRequest;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WhatsappApi
{
    public function __construct(private HttpClientInterface $whatsappBusinessApiClient)
    {
    }

    public function send(SendMessageRequest $whatsappApiRequest): WhatsappApiResponse
    {
        $options = [];
        $options['body'] = $whatsappApiRequest->getBody();

        try {
            $response = $this->whatsappBusinessApiClient->request(
                method: $whatsappApiRequest->getMethod(),
                url: $whatsappApiRequest->getEndpoint(),
                options: $options
            );

            // get the response content
            return $whatsappApiRequest->parseResponseContent($response->getContent());
        } catch (TransportExceptionInterface $e) {
            throw $e;
        }
    }
}
