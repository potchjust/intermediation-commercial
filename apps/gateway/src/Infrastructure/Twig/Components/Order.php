<?php

namespace App\Infrastructure\Twig\Components;

use Symfony\UX\LiveComponent\DefaultActionTrait;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent]
class Order
{
    use DefaultActionTrait;
}
