<?php

namespace App\Infrastructure\Twig\Components;

use App\Context\Catalog\Repository\ProductRepository;
use App\Context\Order\DTO\SupplierOrdersDTO;
use App\Context\Order\Enums\SubOrderStatus;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent('LastTenOrders', template: 'components/Dashboard/LastTenOrders.html.twig')]
class LastTenOrders
{
    use DefaultActionTrait;

    #[LiveProp(hydrateWith: 'hydrateOrdersDTO', dehydrateWith: 'dehydrateOrderDTO')]
    public array $orders;

    public function __construct(
        private readonly ProductRepository $productRepository
    ) {
    }

    public function dehydrateOrderDTO(array $orders)
    {
        return $orders;
    }

}
