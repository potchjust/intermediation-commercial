<?php

// afin de rendre le changements de donnee dynamiques on

namespace App\Infrastructure\Twig\Components;

use App\Context\Order\DTO\OrderDetailsDTO;
use App\Context\Order\Entity\OrderDetails;
use App\Context\Order\Enums\OrderDetailsStatus;
use App\Context\Order\Repository\OrderDetailsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Uid\UuidV7;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveArg;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent]
class OrderDetailsItem
{
    // on va essayer de determiner les valeurs qui seront dynamiques
    use DefaultActionTrait;

    #[LiveProp(hydrateWith: 'hydrateOrderDetailsDTO', dehydrateWith: 'dehydrateOrderDetailsDTO')]
    private OrderDetailsDTO $details;

    public function __construct(private readonly OrderDetailsRepository $orderDetailsRepository, private readonly EntityManagerInterface $entityManager)
    {
    }

    public function getDetails(): OrderDetailsDTO
    {
        return $this->details;
    }

    public function setDetails(OrderDetailsDTO $details): void
    {
        $this->details = $details;
    }

    public function hydrateOrderDetailsDTO(array $details)
    {
        return new OrderDetailsDTO($this->orderDetailsRepository->findOneBy(['id' => UuidV7::fromString($details['id'])]));
    }

    public function dehydrateOrderDetailsDTO(OrderDetailsDTO $details)
    {
        return [
            'id' => $details->getId(),
            'productName' => $details->getProductName(),
            'quantity' => $details->getQuantity(),
            'unitPrice' => $details->getUnitPrice(),
            'subTotal' => $details->getSubTotal(),
            'isInStock' => $details->getIsInStock(),
            'alternatives' => $details->getAlternativesItems(),
        ];
    }

    #[LiveAction]
    public function validateLineItem(#[LiveArg] UuidV7 $id)
    {
        $orderDetails = $this->orderDetailsRepository->findOneBy(['id' => UuidV7::fromString($id)]);
        assert($orderDetails instanceof OrderDetails);
        $orderDetails->setStatus(OrderDetailsStatus::VALIDATED->name);
        $this->entityManager->persist($orderDetails);
        $this->entityManager->flush();
        $this->details = new OrderDetailsDTO($orderDetails);
    }

    public function noticeLineItemChanges()
    {
    }
}
