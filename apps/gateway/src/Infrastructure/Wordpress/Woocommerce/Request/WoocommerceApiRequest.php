<?php

namespace App\Infrastructure\Wordpress\Woocommerce\Request;

abstract class WoocommerceApiRequest
{
    public function __construct(private readonly string $endpoint)
    {
    }

    public function getUrl(): string
    {
        return $this->endpoint;
    }

    abstract public function getMethod();
}
