<?php

namespace App\Infrastructure\Wordpress\Woocommerce\Request;

class WoocommerceSendGetRequest extends WoocommerceApiRequest
{
    public function getMethod(): string
    {
        return 'GET';
    }
}
