<?php

namespace App\Infrastructure\Wordpress\Woocommerce\Request;

class WoocommerceSendPostRequest extends WoocommerceApiRequest
{
    private array $bodyParameters;

    public function getMethod(): string
    {
        return 'POST';
    }

    public function getBody(): array
    {
        return $this->bodyParameters;
    }

    public function addParameter(mixed $parameter): void
    {
        $this->bodyParameters[] = $parameter;
    }

    public function flushParameters(): void
    {
        $this->bodyParameters = [];
    }
}
