<?php

namespace App\Infrastructure\Wordpress\Woocommerce\Request;

class WoocommerceSendDeleteRequest extends WoocommerceApiRequest
{
    private array $bodyParameters;

    public function getMethod(): string
    {
        return 'DELETE';
    }

    public function getBody(): array
    {
        return $this->bodyParameters;
    }

    public function addParameter(mixed $parameter): void
    {
        $this->bodyParameters[] = $parameter;
    }

    public function flushParameters(): void
    {
        $this->bodyParameters = [];
    }
}
