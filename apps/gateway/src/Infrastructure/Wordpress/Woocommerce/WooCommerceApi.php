<?php

namespace App\Infrastructure\Wordpress\Woocommerce;

use App\Infrastructure\Wordpress\Woocommerce\Request\WoocommerceApiRequest;
use Automattic\WooCommerce\Client;
use http\Exception\UnexpectedValueException;

class WooCommerceApi
{
    private readonly string $baseUri;
    private readonly string $consumerKey;
    private readonly string $consumerSecret;
    private ?Client $woocommerceClient = null;

    public function __construct(string $baseUri, string $consumerKey, string $consumerSecret)
    {
        $this->baseUri = trim($baseUri, '/');
        $this->consumerKey = $consumerKey;
        $this->consumerSecret = $consumerSecret;
        if (null == $this->woocommerceClient) {
            $this->woocommerceClient = new Client($baseUri, $this->consumerKey, $this->consumerSecret, [
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
            ]);
        }
    }

    public function send(WoocommerceApiRequest $wooCommerceApiRequest): string
    {
        assert($this->woocommerceClient instanceof Client);
        switch ($wooCommerceApiRequest->getMethod()) {
            case 'GET':
                $this->woocommerceClient->get($wooCommerceApiRequest->getUrl());
                break;
            case 'POST':
                $this->woocommerceClient->post($wooCommerceApiRequest->getUrl(), ...$wooCommerceApiRequest->getBody());
                break;
            case 'PUT':
                $this->woocommerceClient->put($wooCommerceApiRequest->getUrl(), ...$wooCommerceApiRequest->getBody());
                break;
            case 'DELETE':
                $this->woocommerceClient->delete($wooCommerceApiRequest->getUrl(), ...$wooCommerceApiRequest->getBody());
                break;
            default:
                throw new UnexpectedValueException('The current request is not yet implemented');
        }

        // get the lastest request
        return $this->woocommerceClient->http->getResponse()->getBody();
    }
}
