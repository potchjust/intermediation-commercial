<?php

namespace App\Context\Invoice\UseCases;

use App\Context\Order\Entity\Order;
use App\Context\Order\Entity\OrderDetails;
use App\Context\Order\Entity\SubOrder;
use App\Context\Order\Enums\OrderDetailsStatus;
use App\Context\Order\Enums\SubOrderStatus;

class GetSingleOrder
{
    public function get(Order $order)
    {
        $orderItems = [];
        // get the subOrders
        foreach ($order->getSubOrders()->toArray() as $subOrder) {
            assert($subOrder instanceof SubOrder);
            if ($subOrder->getStatus() === SubOrderStatus::VALIDATED->name) {
                // get the order detauls
                $orderDetailsItems = $subOrder->getOrderDetails();
                foreach ($orderDetailsItems as $orderDetailsItem) {
                    assert($orderDetailsItem instanceof OrderDetails);
                    if ($orderDetailsItem->getStatus() === OrderDetailsStatus::VALIDATED->name || $orderDetailsItem->getStatus() === OrderDetailsStatus::CHANGED->name) {
                        $orderItems[] = $orderDetailsItem;
                    }
                }
            }
        }

        return [
            'orderedAt' => $order->getOrderedAt()->format('Y-m-d H:i:s'),
            'customer' => $order->getCustomer()->getDisplayedName(),
            'reference' => $order->getOrderReference(),
            'detailsItems' => $orderItems,
        ];
    }
}
