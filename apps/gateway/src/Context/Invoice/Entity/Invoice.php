<?php

namespace App\Context\Invoice\Entity;

use App\Context\Order\Entity\Order;
use App\Context\Order\Entity\SubOrder;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'invoices')]
class Invoice
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator('doctrine.uuid_generator')]
    private int $id;

    #[ORM\Column(type: Types::TEXT)] // the link will be an amazon s3 stockage
    private string $invoiceLink;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private \DateTimeImmutable $generatedAt;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private \DateTimeImmutable $sendAt;

    #[ORM\ManyToOne(targetEntity: Order::class, inversedBy: 'invoices')]
    private ?Order $order = null;

    #[ORM\ManyToOne(targetEntity: SubOrder::class, inversedBy: 'invoices')]
    private ?SubOrder $subOrder = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getInvoiceLink(): string
    {
        return $this->invoiceLink;
    }

    public function setInvoiceLink(string $invoiceLink): void
    {
        $this->invoiceLink = $invoiceLink;
    }

    public function getGeneratedAt(): \DateTimeImmutable
    {
        return $this->generatedAt;
    }

    public function setGeneratedAt(\DateTimeImmutable $generatedAt): void
    {
        $this->generatedAt = $generatedAt;
    }

    public function getSendAt(): \DateTimeImmutable
    {
        return $this->sendAt;
    }

    public function setSendAt(\DateTimeImmutable $sendAt): void
    {
        $this->sendAt = $sendAt;
    }

    public function getSubOrder(): ?SubOrder
    {
        return $this->subOrder;
    }

    public function setSubOrder(?SubOrder $subOrder): void
    {
        $this->subOrder = $subOrder;
    }
}
