<?php

namespace App\Context\Whatsapp\UseCase\Whatsapp;

use App\Context\Auth\Entity\Customer;
use App\Context\Catalog\Entity\Product;
use App\Context\Catalog\Repository\ProductRepository;
use App\Context\Catalog\Repository\SupplierRepository;
use App\Context\Order\Entity\Order;
use App\Context\Order\Entity\OrderDetails;
use App\Context\Order\Entity\SubOrder;
use App\Context\Order\Enums\OrderDetailsStatus;
use App\Context\Order\Enums\OrderStatus;
use App\Context\Order\Enums\SubOrderStatus;
use App\Context\Order\Repository\OrderRepository;
use App\Context\Order\Repository\SubOrderRepository;
use App\Context\Whatsapp\Dto\Messages\Template\WhatsappMessageTemplateComponentsParameters;
use App\Context\Whatsapp\Dto\Webhook\WhatsappWebhookOrder;
use App\Context\Whatsapp\Enums\WhatsappMessageParametersType;
use App\Context\Whatsapp\UseCase\Request\SendProcessingMessage;
use App\Context\Whatsapp\UseCase\Request\SendTemplateMessage;
use App\Context\Whatsapp\UseCase\Whatsapp\Messages\SendMessageRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Uid\UuidV7;

class WhatsappOrderMessagesHandler
{
    public function __construct(
        private readonly SupplierRepository $supplierRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly ProductRepository $productRepository,
        private readonly SubOrderRepository $subOrderRepository,
        private readonly OrderRepository $orderRepository,
        private SendProcessingMessage $sendProcessingMessage,
        private readonly SendTemplateMessage $sendTemplateMessage)
    {
    }

    /**
     * @return SendMessageRequest[]
     */
    public function handleOrder(WhatsappWebhookOrder $whatsappWebhookOrder, Customer $customer): array
    {
        $serializedData = [];
        $this->sendProcessingMessage->send($customer);
        foreach ($whatsappWebhookOrder->getProductItems() as $productItem) {
            $product = $this->productRepository->getProducyByWoocommerceId($productItem->getWoocommerceProductId());
            assert($product instanceof Product);
            $supplier = $product->getSupplier();
            $supplierId = $supplier->getId()->jsonSerialize();

            if (!array_key_exists($supplier->getId()->jsonSerialize(), $serializedData)) {
                $serializedData[$supplierId] = [];
            }
            if (array_key_exists($supplier->getId()->jsonSerialize(), $serializedData)) {
                $serializedData[$supplierId][] = [
                    'productId' => $product->getId()->jsonSerialize(),
                    'quantity' => $productItem->getQuantity(),
                    'price' => $product->getPrice(),
                ];
            }
        }
        $order = new Order();
        $order->setStatus(OrderStatus::NEW->name);
        $order->setCustomer($customer);
        $order->setOrderedAt(new \DateTimeImmutable());
        $order->setOrderReference($this->getOrderReference());

        // file_put_contents('dsd.json',json_encode($serializedData,JSON_PRETTY_PRINT));

        foreach ($serializedData as $supplierId => $productItems) {
            $supplier = $this->supplierRepository->findOneBy(['id' => UuidV7::fromString($supplierId)]);
            if (null !== $supplier) {
                $subOrder = new SubOrder();
                $subOrder->setStatus(SubOrderStatus::NEW->name);
                $subOrder->setSupplier($supplier);
                // create a orderItems =
                foreach ($productItems as $productItem) {
                    $product = $this->productRepository->findOneBy(['id' => UuidV7::fromString($productItem['productId'])]);
                    $orderDetails = new OrderDetails();
                    $orderDetails->setQuantity($productItem['quantity']);
                    $orderDetails->setStatus(OrderDetailsStatus::PENDING->name); // en cours de traitement;
                    $orderDetails->setProduct($product);
                    $this->entityManager->persist($orderDetails);
                    $subOrder->addOrderDetails($orderDetails);
                }
                $subOrder->setOrderReference($this->generateId($subOrder));
                $this->entityManager->persist($subOrder);
                $order->addSubOrder($subOrder);
            }
        }
        $this->entityManager->persist($order);
        $this->entityManager->flush();

        return [$this->confirmOrderMessage($order)];
    }

    private function getOrderReference(): string
    {
        $allOrders = $this->orderRepository->findAll();
        $count = count($allOrders);
        $nextNumber = $count + 1;

        return sprintf('REF-ORD-%03d', $nextNumber);
    }

    public function generateId(SubOrder $entity): mixed
    {
        $allOrders = $this->subOrderRepository->findBy(['supplier' => $entity->getSupplier()->getId()]);
        $count = count($allOrders);
        $nextNumber = $count + 1;

        return sprintf('REF-SUBORD-%s-%03d', strtoupper($entity->getSupplier()->getSubdomain()), $nextNumber);
    }

    private function confirmOrderMessage(Order $order): SendMessageRequest
    {
        // Début de la table
        $orderTable = '| Nom du Produit | Quantité | Prix    |\\n';
        $orderTable .= '| -------------- | -------- | ------ |\\n';
        $totalPrice = 0;
        foreach ($order->getSubOrders() as $subOrder) {
            assert($subOrder instanceof SubOrder);
            foreach ($subOrder->getOrderDetails() as $orderItem) {
                assert($orderItem instanceof OrderDetails);
                $price = $orderItem->getProduct()->getPrice() * $orderItem->getQuantity();
                $totalPrice += $price;
                $orderTable .= '| '.$orderItem->getProduct()->getLabel().' | '.strval($orderItem->getQuantity()).' | '.strval($price).' |\\n';
                $orderTable .= '| -------------- | -------- | ------ \\n';
            }
        }

        $orderTableComponentParameter = new WhatsappMessageTemplateComponentsParameters();
        $orderTableComponentParameter->setType(WhatsappMessageParametersType::TEXT->value);
        $orderTableComponentParameter->setText($orderTable);
        // order
        $orderedAtComponentParameter = new WhatsappMessageTemplateComponentsParameters();
        $orderedAtComponentParameter->setType(WhatsappMessageParametersType::TEXT->value);
        $orderedAtComponentParameter->setText($order->getOrderedAt()->format('Y-m-d H:i:s'));
        // phoneNumber
        $phoneNumberComponentParameter = new WhatsappMessageTemplateComponentsParameters();
        $phoneNumberComponentParameter->setType(WhatsappMessageParametersType::TEXT->value);
        $phoneNumberComponentParameter->setText($order->getCustomer()->getPhoneNumber());

        // order reference
        $orderReferenceComponentParameter = new WhatsappMessageTemplateComponentsParameters();
        $orderReferenceComponentParameter->setType(WhatsappMessageParametersType::TEXT->value);
        $orderReferenceComponentParameter->setText($order->getOrderReference());

        $priceReferenceComponentParameter = new WhatsappMessageTemplateComponentsParameters();
        $priceReferenceComponentParameter->setType(WhatsappMessageParametersType::TEXT->value);

        $priceReferenceComponentParameter->setText(strval($totalPrice));

        return $this->sendTemplateMessage->createRequest(
            customer: $order->getCustomer(),
            templateName: 'order_details',
            bodyParameters: [
                $orderTableComponentParameter,
                $orderedAtComponentParameter,
                $phoneNumberComponentParameter,
                $orderReferenceComponentParameter,
                $priceReferenceComponentParameter,
            ]
        );
    }
}
