<?php

namespace App\Context\Whatsapp\UseCase\Whatsapp\Messages;

use App\Context\Whatsapp\Dto\Messages\WhatsappMessage;
use App\Context\Whatsapp\Dto\Response\WhatsappApiResponse;
use App\Context\Whatsapp\UseCase\Request\WhatsappApiRequest;
use Symfony\Component\Serializer\SerializerInterface;

class SendMessageRequest extends WhatsappApiRequest
{
    private WhatsappMessage $whatsappMessage;

    public function __construct(private readonly SerializerInterface $messageSerializer)
    {
        parent::__construct($this->messageSerializer);
    }

    public function getEndpoint(): string
    {
        return 'messages';
    }

    public function getMethod(): string
    {
        return 'POST';
    }

    public function setBody(WhatsappMessage $whatsappMessage): void
    {
        $this->whatsappMessage = $whatsappMessage;
    }

    public function getBody(): string
    {
        return $this->messageSerializer->serialize($this->getMessageInstance(), 'json');
    }

    public function parseResponseContent(string $body): WhatsappApiResponse
    {
        return new WhatsappApiResponse();
    }

    public function getMessageInstance(): WhatsappMessage
    {
        return $this->whatsappMessage;
    }
}
