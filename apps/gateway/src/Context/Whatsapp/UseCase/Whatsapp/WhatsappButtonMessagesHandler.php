<?php

namespace App\Context\Whatsapp\UseCase\Whatsapp;

use App\Context\Auth\Entity\Customer;
use App\Context\Order\UseCase\CancelCustomerOrder;
use App\Context\Order\UseCase\ValidateCustomerOrder;
use App\Context\Payment\UseCase\PayOrder;
use App\Context\Whatsapp\Dto\Webhook\WhatsappWebhookButton;
use App\Context\Whatsapp\Enums\UsersActions;

class WhatsappButtonMessagesHandler
{
    public function __construct(private readonly ValidateCustomerOrder $validateCustomerOrder,
        private readonly PayOrder $payOrder,
        private readonly CancelCustomerOrder $cancelCustomerOrder)
    {
    }

    /**
     * @throws \Exception
     */
    public function handleButton(WhatsappWebhookButton $whatsappWebhookButton, Customer $customer): array
    {
        $action = UsersActions::from(trim($whatsappWebhookButton->getAction()))->name;
        $orderReference = trim($whatsappWebhookButton->getPayload());

        $requests[] = match ($action) {
            'CANCEL_ORDER' => $this->cancelCustomerOrder->cancel($orderReference, $customer),
            'VALIDATE_ORDER' => $this->validateCustomerOrder->validate($orderReference, $customer),
            'PAY_ORDER' => $this->payOrder->pay($orderReference, $customer),// todo write a payment validation payment
            default => throw new \Exception(sprintf('Invalid action %s', $action)),
        };

        return $requests;
    }
}
