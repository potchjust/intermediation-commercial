<?php

namespace App\Context\Whatsapp\UseCase\Whatsapp;

use App\Context\Auth\Entity\Customer;
use App\Context\Order\UseCase\GetCustomerOrders;
use App\Context\Order\UseCase\GetSingleOrderDetails;
use App\Context\Payment\UseCase\PayOrder;
use App\Context\Whatsapp\Dto\Messages\Text\WhatsappTextMessage;
use App\Context\Whatsapp\Dto\Webhook\WhatsappWebhookMessages;
use App\Context\Whatsapp\Entity\ActionTrails;
use App\Context\Whatsapp\Enums\UsersActions;
use App\Context\Whatsapp\Enums\WhatsappMessageType;
use App\Context\Whatsapp\UseCase\Request\SendMenuMessage;
use App\Context\Whatsapp\UseCase\Request\SendProcessingMessage;
use App\Context\Whatsapp\UseCase\Whatsapp\Messages\SendMessageRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;

final readonly class WhatsappTextMessagesHandler
{
    public function __construct(
        private SerializerInterface $serializer,
        private SendMenuMessage $sendMenuMessage,
        private GetCustomerOrders $getCustomerOrders,
        private GetSingleOrderDetails $getSingleOrderDetails,
        private PayOrder $payOrder,
        private SendProcessingMessage $processingMessage,
        private EntityManagerInterface $entityManager)
    {
    }

    /**
     *  I want my function to return a request.
     */
    public function handle(WhatsappWebhookMessages $whatsappWebhookMessages, Customer $customer): array
    {
        $bodyMessage = $whatsappWebhookMessages->getTextBody();
        $action = trim($bodyMessage);
        $requests = [];
        // create the actions
        switch ($action) {
            case 'Bonjour':
            case 'Menu':
            case 'Aide':
                $requests[] = $this->sendMenuMessage->getRequests($customer);
                $actionTrails = new ActionTrails();
                $actionTrails->addNewCustomer($customer);
                $actionTrails->setAction($action);
                $actionTrails->setContext(UsersActions::MENU->name);
                $this->entityManager->persist($actionTrails);
                $this->entityManager->flush();
                break;
            case '1':
                $this->processingMessage->send($customer);
                $requests[] = $this->getCustomerOrders->get($customer);
                $actionTrails = new ActionTrails();
                $actionTrails->addNewCustomer($customer);
                $actionTrails->setAction($action);
                $actionTrails->setContext(UsersActions::LIST_ALL_ORDERS->name);
                $this->entityManager->persist($actionTrails);
                $this->entityManager->flush();
                break;
            case '2':
            case '3':
                $requests[] = $this->getSingleOrderDetails->askUserToEnterTheReference($customer);
                $actionTrails = new ActionTrails();
                $actionTrails->addNewCustomer($customer);
                $actionTrails->setAction($action);
                $actionTrails->setContext(UsersActions::GET_ORDER_ASK_REFERENCE->name);
                $this->entityManager->persist($actionTrails);
                $this->entityManager->flush();
                break;
            default:
                $requests[] = $this->handleOtherText($customer, $bodyMessage);
                break;
        }

        return $requests;
    }

    private function handleOtherText(Customer $customer, string $bodyMessage)
    {
        $userActions = $customer->getActionTrails();
        $orderReference = trim($bodyMessage);
        if (str_contains($orderReference, 'REF-ORD')) {
            if ($userActions->count() > 0) {
                $lastUserAction = $userActions->last();
                assert($lastUserAction instanceof ActionTrails);
                // handle single orders details
                if ($lastUserAction->getContext() === UsersActions::GET_ORDER_ASK_REFERENCE->name) {
                    $this->processingMessage->send($customer);
                    switch ($lastUserAction->getAction()) {
                        case '2':
                            $singleOrderDetails = $this->getSingleOrderDetails->handleSingleOrderReference(orderReference: $orderReference);
                            $actionTrail = new ActionTrails();
                            $actionTrail->setAction($lastUserAction->getAction());
                            $actionTrail->setParent($lastUserAction->getParent());
                            $actionTrail->addNewCustomer($customer);
                            $actionTrail->setContext(UsersActions::GET_ORDER->name);
                            $this->entityManager->persist($actionTrail);
                            $this->entityManager->flush();

                            return $singleOrderDetails;
                        case '3':
                            $payOrder = $this->payOrder->pay($customer, $lastUserAction->getContext());
                            $actionTrail = new ActionTrails();
                            $actionTrail->setAction($lastUserAction->getAction());
                            $actionTrail->setParent($lastUserAction->getParent());
                            $actionTrail->addNewCustomer($customer);
                            $actionTrail->setContext(UsersActions::PAY_ORDER->name);
                            $this->entityManager->persist($actionTrail);
                            $this->entityManager->flush();

                            return $payOrder;
                        default:
                            return $this->uncomprehensibleTextMessage($customer);
                    }
                }
            }
        }

        return $this->uncomprehensibleTextMessage($customer);
    }

    private function uncomprehensibleTextMessage(Customer $customer): SendMessageRequest
    {
        $textMessage = new WhatsappTextMessage();
        $textMessage->setMessagingProduct('whatsapp');
        $textMessage->setTo($customer->getPhoneNumber());
        $textMessage->setType(WhatsappMessageType::TEXT->value);
        $formattedMessage = 'Cher utilisateur ,Je crains de ne pas avoir compris votre message. Pour vous aider au mieux, veuillez utiliser des mots-clés pris en charge.Voici quelques exemples de mots-clés que je peux comprendre : Bonjour, Menu , Aide';
        $textMessage->setContent([
            'preview_url' => false,
            'body' => $formattedMessage,
        ]);
        $request = new SendMessageRequest($this->serializer);
        $request->setBody($textMessage);

        return $request;
    }
}
