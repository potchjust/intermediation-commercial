<?php

namespace App\Context\Whatsapp\UseCase\Request;

use App\Context\Auth\Entity\Customer;
use App\Context\Whatsapp\Dto\Messages\Template\WhatsappMessageTemplate;
use App\Context\Whatsapp\Dto\Messages\Template\WhatsappMessageTemplateComponents;
use App\Context\Whatsapp\Enums\WhatsappMessageComponentsType;
use App\Context\Whatsapp\Enums\WhatsappMessageType;
use App\Context\Whatsapp\UseCase\Whatsapp\Messages\SendMessageRequest;
use Symfony\Component\Serializer\SerializerInterface;

final class SendTemplateMessage
{
    public function __construct(private readonly SerializerInterface $serializer,
    ) {
    }

    private function getTemplateMessage(Customer $customer, string $templateName,
        array $bodyParameters = [],
        array $components = []): WhatsappMessageTemplate
    {
        // Catalog message
        $templateMessage = new WhatsappMessageTemplate();
        $templateMessage->setTo($customer->getPhoneNumber());
        $templateMessage->setType(WhatsappMessageType::TEMPLATE->value);
        $templateMessage->setMessagingProduct('whatsapp');
        $templateMessage->setName($templateName);
        $templateMessage->setLangage([
            'language' => [
                'code' => 'fr',
            ],
        ]);
        // create the body components
        $bodyComponent = new WhatsappMessageTemplateComponents();
        $bodyComponent->setType(WhatsappMessageComponentsType::BODY->value);
        $bodyComponent->setParameters($bodyParameters);
        if (count($components) > 0) {
            foreach ($components as $component) {
                $templateMessage->addComponent($component);
            }
        } else {
            $templateMessage->setComponents([]);
        }
        $templateMessage->addComponent($bodyComponent);

        return $templateMessage;
    }

    public function createRequest(Customer $customer,
        string $templateName,
        array $bodyParameters = [],
        array $components = []): SendMessageRequest
    {
        $templateMessageRequest = new SendMessageRequest($this->serializer);
        $templateMessageRequest->setBody($this->getTemplateMessage($customer, $templateName, $bodyParameters, $components));

        return $templateMessageRequest;
    }
}
