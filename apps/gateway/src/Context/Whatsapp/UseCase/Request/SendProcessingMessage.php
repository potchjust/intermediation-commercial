<?php

namespace App\Context\Whatsapp\UseCase\Request;

use App\Context\Auth\Entity\Customer;
use App\Context\Whatsapp\Dto\Messages\Text\WhatsappTextMessage;
use App\Context\Whatsapp\Enums\WhatsappMessageType;
use App\Context\Whatsapp\UseCase\Whatsapp\Messages\SendMessageRequest;
use App\Infrastructure\Whatsapp\WhatsappApi;
use Symfony\Component\Serializer\SerializerInterface;

class SendProcessingMessage
{
    public function __construct(private SerializerInterface $serializer, private readonly WhatsappApi $whatsappApi)
    {
    }

    public function send(Customer $customer): void
    {
        $textMessage = new WhatsappTextMessage();
        $textMessage->setMessagingProduct('whatsapp');
        $textMessage->setTo($customer->getPhoneNumber());
        $textMessage->setType(WhatsappMessageType::TEXT->value);
        $formattedMessage = "Merci de patienter. Votre requête est en cours de traitement.Les délais d'attente pour chaque opération varient de 1 à 5 minutes en fonction de l'opération";
        $textMessage->setContent([
            'preview_url' => false,
            'body' => $formattedMessage,
        ]);
        $request = new SendMessageRequest($this->serializer);
        $request->setBody($textMessage);
        $this->whatsappApi->send($request);
    }
}
