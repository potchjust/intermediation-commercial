<?php

namespace App\Context\Whatsapp\UseCase\Request;

use App\Context\Whatsapp\Dto\Response\WhatsappApiResponse;
use Symfony\Component\Serializer\SerializerInterface;

abstract class WhatsappApiRequest
{
    public function __construct(private readonly SerializerInterface $messageSerializer)
    {
    }

    abstract public function getEndpoint(): string;

    abstract public function getMethod(): string;

    abstract public function parseResponseContent(string $body): WhatsappApiResponse;
}
