<?php

namespace App\Context\Whatsapp\UseCase\Request;

use App\Context\Auth\Entity\Customer;
use App\Context\Whatsapp\Dto\Messages\Template\WhatsappMessageTemplate;
use App\Context\Whatsapp\Dto\Messages\Template\WhatsappMessageTemplateComponents;
use App\Context\Whatsapp\Dto\Messages\Template\WhatsappMessageTemplateComponentsParameters;
use App\Context\Whatsapp\Enums\WhatsappMessageComponentsSubType;
use App\Context\Whatsapp\Enums\WhatsappMessageComponentsType;
use App\Context\Whatsapp\Enums\WhatsappMessageParametersType;
use App\Context\Whatsapp\Enums\WhatsappMessageType;
use App\Context\Whatsapp\UseCase\Whatsapp\Messages\SendMessageRequest;
use Symfony\Component\Serializer\SerializerInterface;

class SendMenuMessage
{
    public function __construct(private SerializerInterface $serializer)
    {
    }

    public function getCatalogMessage(Customer $customer): WhatsappMessageTemplate
    {
        // Catalog message
        $catalogueMessageTemplate = new WhatsappMessageTemplate();
        $catalogueMessageTemplate->setTo($customer->getPhoneNumber());
        $catalogueMessageTemplate->setType(WhatsappMessageType::TEMPLATE->value);
        $catalogueMessageTemplate->setMessagingProduct('whatsapp');
        $catalogueMessageTemplate->setName('greetings');
        $catalogueMessageTemplate->setLangage([
            'language' => [
                'code' => 'fr',
            ],
        ]);

        // add components
        // headers
        $headerComponentParameterImage = new WhatsappMessageTemplateComponentsParameters();
        $headerComponentParameterImage->setType(WhatsappMessageParametersType::IMAGE->value);
        $headerComponentParameterImage->setAction([
            'link' => 'https://c8.alamy.com/compfr/e1k500/grossiste-riz-e1k500.jpg',
        ]);
        $headerComponent = new WhatsappMessageTemplateComponents();
        $headerComponent->setType(WhatsappMessageComponentsType::HEADER->value);
        $headerComponent->addParameter($headerComponentParameterImage);

        // create the body components
        $bodyComponent = new WhatsappMessageTemplateComponents();
        $bodyComponent->setType(WhatsappMessageComponentsType::BODY->value);
        $bodyComponent->setParameters([]);

        $buttonComponent = new WhatsappMessageTemplateComponents();
        $buttonComponent->setType(WhatsappMessageComponentsType::BUTTON->value);
        $buttonComponent->setSubType(WhatsappMessageComponentsSubType::CATALOG->value);
        $buttonComponent->setIndex(0);

        // button component parameter

        $catalogComponentParameter = new WhatsappMessageTemplateComponentsParameters();
        $catalogComponentParameter->setType(WhatsappMessageParametersType::ACTION->value);
        $catalogComponentParameter->setAction([
            'thumbnail_product_retailer_id' => 'vdtfsjmlyl',
        ]);
        // add element in their parent
        $buttonComponent->addParameter($catalogComponentParameter);
        // $catalogueMessageTemplate->addComponent($headerComponent);
        $catalogueMessageTemplate->addComponent($bodyComponent);
        $catalogueMessageTemplate->addComponent($buttonComponent);

        return $catalogueMessageTemplate;
    }

    public function getRequests(Customer $customer): SendMessageRequest
    {
        $catalogMessageRequest = new SendMessageRequest($this->serializer);
        $catalogMessageRequest->setBody($this->getCatalogMessage($customer));

        return $catalogMessageRequest;
    }
}
