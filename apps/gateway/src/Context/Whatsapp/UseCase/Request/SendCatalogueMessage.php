<?php

namespace App\Context\Whatsapp\UseCase\Request;

use App\Context\Whatsapp\Dto\Messages\WhatsappMessage;
use App\Context\Whatsapp\Dto\Response\WhatsappApiResponse;
use App\Infrastructure\Whatsapp\Request\CatalogueMessageRequestRequest;
use App\Infrastructure\Whatsapp\WhatsappApi;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class SendCatalogueMessage
{
    public function __construct(private readonly WhatsappApi $whatsappApi, private SerializerInterface $serializer)
    {
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendMessage(WhatsappMessage $whatsappMessage): WhatsappApiResponse
    {
        return $this->whatsappApi->send(new CatalogueMessageRequestRequest($whatsappMessage, $this->serializer));
    }
}
