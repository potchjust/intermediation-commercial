<?php

namespace App\Context\Whatsapp\Enums;

/**
 * Class that will define the type of message we have.
 **/
enum UsersActions: string
{
    case MENU = 'Menu';
    case LIST_ALL_ORDERS = 'Lister les commandes';
    case GET_ORDER_ASK_REFERENCE = 'demande à l\'utilisateur la reference';
    case GET_ORDER = 'Afficher la commande';
    case VALIDATE_ORDER = 'Valider la commande';
    case CANCEL_ORDER = 'Annuler la commande';
    case PAY_ORDER = 'Payer la commande';
    case OTHER = 'Aucune';
}
