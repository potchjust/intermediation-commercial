<?php

namespace App\Context\Whatsapp\Enums;

/**
 * Class that will define the type of message we have.
 **/
enum WhatsappMessageType: string
{
    case TEMPLATE = 'template';
    case TEXT = 'text';
    case INTERACTIVE = 'interactive';
}
