<?php

namespace App\Context\Whatsapp\Enums;

enum WhatsappMessageParametersType: string
{
    case ACTION = 'action';
    case IMAGE = 'image';
    case PAYLOAD = 'payload';
    case BUTTON = 'button';
    case CATALOGUE = 'catalog';
    case TEXT = 'text';
}
