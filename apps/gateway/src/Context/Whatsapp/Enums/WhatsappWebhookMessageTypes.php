<?php

namespace App\Context\Whatsapp\Enums;

enum WhatsappWebhookMessageTypes: string
{
    case TEXT = 'text';
    case ORDER = 'order';
    case IMAGE = 'image';
    case BUTTON = 'button';
}
