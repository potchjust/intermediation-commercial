<?php

namespace App\Context\Whatsapp\Enums;

/**
 * Class that will define the type of message we have.
 **/
enum WhatsappMessageComponentsType: string
{
    case HEADER = 'header';
    case BODY = 'body';
    case BUTTON = 'button';
}
