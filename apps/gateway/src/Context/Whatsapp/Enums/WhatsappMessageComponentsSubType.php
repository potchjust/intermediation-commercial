<?php

namespace App\Context\Whatsapp\Enums;

/**
 * Class that will define the type of message we have.
 **/
enum WhatsappMessageComponentsSubType: string
{
    case CATALOG = 'CATALOG';
    case QUICK_REPLY = 'quick_reply';
}
