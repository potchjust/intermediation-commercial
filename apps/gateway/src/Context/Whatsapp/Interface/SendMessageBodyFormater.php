<?php

namespace App\Context\Whatsapp\Interface;

use App\Context\Whatsapp\Dto\Messages\Template\WhatsappMessageTemplate;
use App\Context\Whatsapp\Dto\Messages\Template\WhatsappMessageTemplateComponents;
use App\Context\Whatsapp\Dto\Messages\Template\WhatsappMessageTemplateComponentsParameters;
use App\Context\Whatsapp\Dto\Messages\WhatsappMessage;

interface SendMessageBodyFormater
{
    /**
     * @param WhatsappMessageTemplateComponentsParameters[] $whatsappMessageTemplateComponentsParameters
     */
    public function reformatMessageTemplateComponentParameters(array $whatsappMessageTemplateComponentsParameters): mixed;

    /**
     * @param WhatsappMessageTemplateComponents[] $messageTemplateComponents
     */
    public function reformatMessageTemplateComponents(array $messageTemplateComponents): mixed;

    public function reformatMessageTemplate(WhatsappMessageTemplate $messageTemplate): mixed;

    public function reformatMessage(WhatsappMessage $whatsappMessage): mixed;
}
