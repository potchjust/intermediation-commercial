<?php

namespace App\Context\Whatsapp\Dto\Messages\Interactive;

use App\Context\Whatsapp\Dto\Messages\WhatsappMessage;

class WhatsappInteractiveMessage extends WhatsappMessage
{
    private string $interactiveButtonType;
    private array $bodyText;

    private WhatsappInteractiveMessageAction $whatsappInteractiveMessageAction;

    public function getInteractiveButtonType(): string
    {
        return $this->interactiveButtonType;
    }

    public function setInteractiveButtonType(string $interactiveButtonType): void
    {
        $this->interactiveButtonType = $interactiveButtonType;
    }

    public function getBodyText(): array
    {
        return $this->bodyText;
    }

    public function setBodyText(array $bodyText): void
    {
        $this->bodyText = $bodyText;
    }

    public function getWhatsappInteractiveMessageAction(): WhatsappInteractiveMessageAction
    {
        return $this->whatsappInteractiveMessageAction;
    }

    public function setWhatsappInteractiveMessageAction(WhatsappInteractiveMessageAction $whatsappInteractiveMessageAction): void
    {
        $this->whatsappInteractiveMessageAction = $whatsappInteractiveMessageAction;
    }
}
