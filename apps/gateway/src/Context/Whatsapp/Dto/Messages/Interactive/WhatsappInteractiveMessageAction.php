<?php

namespace App\Context\Whatsapp\Dto\Messages\Interactive;

class WhatsappInteractiveMessageAction
{
    private bool $hasButtonText = false;
    private string $button;
    private array $actionsButtons;

    public function isHasButtonText(): bool
    {
        return $this->hasButtonText;
    }

    public function setHasButtonText(bool $hasButtonText): void
    {
        $this->hasButtonText = $hasButtonText;
    }

    public function getButton(): string
    {
        return $this->button;
    }

    public function setButton(string $button): void
    {
        $this->button = $button;
    }

    public function getActionsButtons(): array
    {
        return $this->actionsButtons;
    }

    public function setActionsButtons(array $actionsButtons): void
    {
        $this->actionsButtons = $actionsButtons;
    }
}
