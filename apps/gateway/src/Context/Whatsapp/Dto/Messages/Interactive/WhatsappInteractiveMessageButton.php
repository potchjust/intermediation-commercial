<?php

namespace App\Context\Whatsapp\Dto\Messages\Interactive;

class WhatsappInteractiveMessageButton
{
    private string $type;

    private array $params;

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function setParams(array $params): void
    {
        $this->params = $params;
    }
}
