<?php

namespace App\Context\Whatsapp\Dto\Messages;

class WhatsappMessage
{
    private string $messagingProduct;
    private string $to;
    private string $type;

    public function getMessagingProduct(): string
    {
        return $this->messagingProduct;
    }

    public function setMessagingProduct(string $messagingProduct): void
    {
        $this->messagingProduct = $messagingProduct;
    }

    public function getTo(): string
    {
        return $this->to;
    }

    public function setTo(string $to): void
    {
        $this->to = $to;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }
}
