<?php

namespace App\Context\Whatsapp\Dto\Messages\Text;

use App\Context\Whatsapp\Dto\Messages\WhatsappMessage;

class WhatsappTextMessage extends WhatsappMessage
{
    /**
     * @var array<string>
     */
    private array $content;

    public function getContent(): array
    {
        return $this->content;
    }

    public function setContent(array $content): void
    {
        $this->content = $content;
    }
}
