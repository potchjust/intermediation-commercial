<?php

namespace App\Context\Whatsapp\Dto\Messages\Response;

use App\Context\Whatsapp\Dto\Messages\Text\WhatsappTextMessage;

class WhatsappTextMessageResponse extends WhatsappTextMessage
{
    private string $context;

    public function getContext(): string
    {
        return $this->context;
    }

    public function setContext(string $context): void
    {
        $this->context = $context;
    }
}
