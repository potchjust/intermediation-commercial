<?php

namespace App\Context\Whatsapp\Dto\Messages\Template;

use App\Context\Whatsapp\Dto\Messages\WhatsappMessage;

class WhatsappMessageTemplate extends WhatsappMessage
{
    private string $name;

    private array $langage = [
        'code' => 'fr',
    ];

    /** @var WhatsappMessageTemplateComponents[] */
    private array $components;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getLangage(): array
    {
        return $this->langage;
    }

    public function setLangage(array $langage): void
    {
        $this->langage = $langage;
    }

    public function getComponents(): array
    {
        return $this->components;
    }

    public function setComponents(array $components): void
    {
        $this->components = $components;
    }

    public function addComponent(WhatsappMessageTemplateComponents $components): void
    {
        $this->components[] = $components;
    }
}
