<?php

namespace App\Context\Whatsapp\Dto\Messages\Template;

class WhatsappMessageTemplateComponents
{
    private string $type;
    private ?string $subType = null;
    private ?int $index = null;

    /** @var WhatsappMessageTemplateComponentsParameters[] */
    private array $parameters;

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function setParameters(array $parameters): void
    {
        $this->parameters = $parameters;
    }

    public function getSubType(): ?string
    {
        return $this->subType;
    }

    public function setSubType(?string $subType): void
    {
        $this->subType = $subType;
    }

    public function getIndex(): ?int
    {
        return $this->index;
    }

    public function setIndex(?int $index): void
    {
        $this->index = $index;
    }

    public function addParameter(WhatsappMessageTemplateComponentsParameters $whatsappMessageTemplateComponentsParameters): void
    {
        $this->parameters[] = $whatsappMessageTemplateComponentsParameters;
    }
}
