<?php

namespace App\Context\Whatsapp\Dto\Messages\Template;

class WhatsappMessageTemplateComponentsParameters
{
    private string $type;
    private array $action;
    private ?string $text;

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getAction(): array
    {
        return $this->action;
    }

    public function setAction(array $action): void
    {
        $this->action = $action;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): void
    {
        $this->text = $text;
    }
}
