<?php

namespace App\Context\Whatsapp\Dto\Webhook;

class WhatsappWebhookOrder
{
    private string $catalogId;
    private string $text;

    /** @var WhatsappWebhookProductItems[] */
    private array $productItems;

    public static function generate(array $data): WhatsappWebhookOrder
    {
        $object = new self();
        $object->setText($data['text']);
        $object->setCatalogId($data['catalog_id']);
        $object->setText($data['text']);
        $productItems = [];
        foreach ($data['product_items'] as $product_item) {
            $productItems[] = WhatsappWebhookProductItems::generate($product_item);
        }
        $object->setProductItems($productItems);

        return $object;
    }

    public function getCatalogId(): string
    {
        return $this->catalogId;
    }

    public function setCatalogId(string $catalogId): void
    {
        $this->catalogId = $catalogId;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }

    public function getProductItems(): array
    {
        return $this->productItems;
    }

    public function setProductItems(array $productItems): void
    {
        $this->productItems = $productItems;
    }
}
