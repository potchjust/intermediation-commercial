<?php

namespace App\Context\Whatsapp\Dto\Webhook;

class WhatsappWebhookStatus
{
    private string $id;
    private string $status;
    private string $timestamps;
    private string $recipientId;

    private WhatsappWebhookStatusConversation $statusConversation;

    public static function generate(array $status): WhatsappWebhookStatus
    {
        $_status = new self();
        $_status->setId($status['id']);
        $_status->setTimestamps($status['timestamp']);
        $_status->setRecipientId($status['recipient_id']);
        if (array_key_exists('conversation', $status)) {
            $_status->setStatusConversation(WhatsappWebhookStatusConversation::generate($status['conversation']));
        }

        return $_status;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function getTimestamps(): string
    {
        return $this->timestamps;
    }

    public function setTimestamps(string $timestamps): void
    {
        $this->timestamps = $timestamps;
    }

    public function getRecipientId(): string
    {
        return $this->recipientId;
    }

    public function setRecipientId(string $recipientId): void
    {
        $this->recipientId = $recipientId;
    }

    public function getStatusConversation(): WhatsappWebhookStatusConversation
    {
        return $this->statusConversation;
    }

    public function setStatusConversation(WhatsappWebhookStatusConversation $statusConversation): void
    {
        $this->statusConversation = $statusConversation;
    }
}
