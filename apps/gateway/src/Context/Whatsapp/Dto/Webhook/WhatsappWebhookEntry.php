<?php

namespace App\Context\Whatsapp\Dto\Webhook;

class WhatsappWebhookEntry
{
    private string $id;

    /** @var WhatsappWebhookChanges[] */
    private array $changes;

    public static function generateWebhookEntry(array $data): WhatsappWebhookEntry
    {
        $object = new static();
        $object->setId($data['id']);
        foreach ($data['changes'] as $change) {
            $object->addNewChange(WhatsappWebhookChanges::generateObject($change));
        }

        return $object;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getChanges(): array
    {
        return $this->changes;
    }

    public function setChanges(array $changes): void
    {
        $this->changes = $changes;
    }

    private function addNewChange(WhatsappWebhookChanges $whatsappWebhookChanges): void
    {
        $this->changes[] = $whatsappWebhookChanges;
    }
}
