<?php

namespace App\Context\Whatsapp\Dto\Webhook;

class WhatsappWebhook
{
    private string $object;
    /** @var WhatsappWebhookEntry[] */
    private array $entries;

    public static function generateObject(array $data): WhatsappWebhook
    {
        $object = new static();
        $object->setObject($data['object']);
        $entries = $data['entry'];
        foreach ($entries as $entry) {
            $object->addEntry(WhatsappWebhookEntry::generateWebhookEntry($entry));
        }

        return $object;
    }

    public function getObject(): string
    {
        return $this->object;
    }

    public function setObject(string $object): void
    {
        $this->object = $object;
    }

    public function getEntries(): array
    {
        return $this->entries;
    }

    public function setEntries(array $entries): void
    {
        $this->entries = $entries;
    }

    public function addEntry(WhatsappWebhookEntry $whatsappWebhookEntry)
    {
        $this->entries[] = $whatsappWebhookEntry;
    }
}
