<?php

namespace App\Context\Whatsapp\Dto\Webhook;

class WhatsappWebhookButton
{
    private string $action;
    private string $payload;

    public static function generate(array $data): WhatsappWebhookButton
    {
        $object = new self();
        $object->setAction($data['text']);
        $object->setPayload($data['payload']);

        return $object;
    }

    public function getPayload(): string
    {
        return $this->payload;
    }

    public function setPayload(string $payload): void
    {
        $this->payload = $payload;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function setAction(string $action): void
    {
        $this->action = $action;
    }
}
