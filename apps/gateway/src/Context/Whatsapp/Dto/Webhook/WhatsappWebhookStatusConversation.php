<?php

namespace App\Context\Whatsapp\Dto\Webhook;

class WhatsappWebhookStatusConversation
{
    private string $id;
    private string $expirationTimestamp;
    private string $origin;

    public static function generate(array $data): WhatsappWebhookStatusConversation
    {
        $conversation = new self();
        $conversation->setId($data['id']);
        if (array_key_exists('expiration_timestamp', $data)) {
            $conversation->setExpirationTimestamp($data['expiration_timestamp']);
        }
        $conversation->setOrigin($data['origin']['type']);

        return $conversation;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getExpirationTimestamp(): string
    {
        return $this->expirationTimestamp;
    }

    public function setExpirationTimestamp(string $expirationTimestamp): void
    {
        $this->expirationTimestamp = $expirationTimestamp;
    }

    public function getOrigin(): string
    {
        return $this->origin;
    }

    public function setOrigin(string $origin): void
    {
        $this->origin = $origin;
    }
}
