<?php

namespace App\Context\Whatsapp\Dto\Webhook;

class WhatsappWebhookProductItems
{
    private string $productId;
    private int $woocommerceProductId;
    private int $quantity;
    private int $itemPrice;
    private string $currency;

    public static function generate(array $data): WhatsappWebhookProductItems
    {
        $product = new self();
        $separatedProductRetailerId = explode('_', $data['product_retailer_id']);
        $product->setCurrency($data['currency']);
        $product->setQuantity($data['quantity']);
        $product->setItemPrice($data['item_price']);
        if (count($separatedProductRetailerId) > 1) {
            $product->setProductId($separatedProductRetailerId[0]);
            $product->setWoocommerceProductId($separatedProductRetailerId[1]);
        }

        return $product;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    public function getItemPrice(): int
    {
        return $this->itemPrice;
    }

    public function setItemPrice(int $itemPrice): void
    {
        $this->itemPrice = $itemPrice;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function setProductId(string $productId): void
    {
        $this->productId = $productId;
    }

    public function getWoocommerceProductId(): int
    {
        return $this->woocommerceProductId;
    }

    public function setWoocommerceProductId(int $woocommerceProductId): void
    {
        $this->woocommerceProductId = $woocommerceProductId;
    }
}
