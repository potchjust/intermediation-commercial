<?php

namespace App\Context\Whatsapp\Dto\Webhook;

class WhatsappWebhookContacts
{
    private string $waId = '';
    private string $profileName = '';

    public static function generate(array $data): WhatsappWebhookContacts
    {
        $contact = new self();
        $contact->setWaId($data['wa_id']);
        $contact->setProfileName($data['profile']['name']);

        return $contact;
    }

    public function getWaId(): string
    {
        return $this->waId;
    }

    public function setWaId(string $waId): void
    {
        $this->waId = $waId;
    }

    public function getProfileName(): string
    {
        return $this->profileName;
    }

    public function setProfileName(string $profileName): void
    {
        $this->profileName = $profileName;
    }
}
