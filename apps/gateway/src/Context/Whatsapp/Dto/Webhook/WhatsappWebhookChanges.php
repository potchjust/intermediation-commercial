<?php

namespace App\Context\Whatsapp\Dto\Webhook;

class WhatsappWebhookChanges
{
    private string $field;
    private WhatsappWebhookValue $webhookValue;

    public static function generateObject(array $data): WhatsappWebhookChanges
    {
        $webhookChange = new self();
        $webhookChange->setField($data['field']);
        $webhookChange->setWebhookValue(WhatsappWebhookValue::generate($data['value']));

        return $webhookChange;
    }

    public function getWebhookValue(): WhatsappWebhookValue
    {
        return $this->webhookValue;
    }

    public function setWebhookValue(WhatsappWebhookValue $webhookValue): void
    {
        $this->webhookValue = $webhookValue;
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function setField(string $field): void
    {
        $this->field = $field;
    }
}
