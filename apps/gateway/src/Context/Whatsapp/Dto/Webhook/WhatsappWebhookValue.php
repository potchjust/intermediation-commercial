<?php

namespace App\Context\Whatsapp\Dto\Webhook;

class WhatsappWebhookValue
{
    private string $messagingProduct = '';
    /** @var WhatsappWebhookContacts[] */
    private array $contacts = [];
    /** @var WhatsappWebhookMessages[] */
    private array $messages = [];

    /** @var WhatsappWebhookStatus[] */
    private array $statuses = [];

    public static function generate(array $data): WhatsappWebhookValue
    {
        $webhookValue = new self();
        $webhookValue->setMessagingProduct($data['messaging_product']);

        if (array_key_exists('statuses', $data)) {
            $webhookValue->setStatuses([WhatsappWebhookStatus::generate($data['statuses'][0])]);
        }
        if (array_key_exists('messages', $data)) {
            $webhookValue->setMessages([WhatsappWebhookMessages::generate($data['messages'][0])]);
        }
        if (array_key_exists('contacts', $data)) {
            $webhookValue->setContacts([WhatsappWebhookContacts::generate($data['contacts'][0])]);
        }

        return $webhookValue;
    }

    public function getMessagingProduct(): string
    {
        return $this->messagingProduct;
    }

    public function setMessagingProduct(string $messagingProduct): void
    {
        $this->messagingProduct = $messagingProduct;
    }

    public function getContacts(): array
    {
        return $this->contacts;
    }

    public function setContacts(array $contacts): void
    {
        $this->contacts = $contacts;
    }

    public function getMessages(): array
    {
        return $this->messages;
    }

    public function setMessages(array $messages): void
    {
        $this->messages = $messages;
    }

    public function getStatuses(): array
    {
        return $this->statuses;
    }

    public function setStatuses(array $statuses): void
    {
        $this->statuses = $statuses;
    }
}
