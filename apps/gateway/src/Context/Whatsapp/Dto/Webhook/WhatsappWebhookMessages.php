<?php

namespace App\Context\Whatsapp\Dto\Webhook;

use App\Context\Whatsapp\Enums\WhatsappWebhookMessageTypes;

class WhatsappWebhookMessages
{
    private string $from;
    private string $id;
    private string $timeStamps;
    private string $textBody;
    private string $type;
    private ?WhatsappWebhookOrder $whatsappWebhookOrder = null;
    private ?WhatsappWebhookButton $whatsappWebhookButton = null;

    public static function generate(array $data): WhatsappWebhookMessages
    {
        $object = new self();
        $object->setId($data['id']);
        $object->setFrom($data['from']);
        $object->setTimeStamps($data['timestamp']);
        $object->setType($data['type']);
        switch ($data['type']) {
            case WhatsappWebhookMessageTypes::ORDER->value :
                $object->setWhatsappWebhookOrder(WhatsappWebhookOrder::generate($data['order']));
                break;
            case WhatsappWebhookMessageTypes::TEXT->value:
                $object->setTextBody($data['text']['body']);
                break;
            case WhatsappWebhookMessageTypes::BUTTON->value:
                $object->setWhatsappWebhookButton(WhatsappWebhookButton::generate($data['button']));
                break;
            default:
                throw new \UnexpectedValueException($data['type']);
        }

        return $object;
    }

    public function getFrom(): string
    {
        return $this->from;
    }

    public function setFrom(string $from): void
    {
        $this->from = $from;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getTimeStamps(): string
    {
        return $this->timeStamps;
    }

    public function setTimeStamps(string $timeStamps): void
    {
        $this->timeStamps = $timeStamps;
    }

    public function getTextBody(): string
    {
        return $this->textBody;
    }

    public function setTextBody(string $textBody): void
    {
        $this->textBody = $textBody;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getWhatsappWebhookOrder(): ?WhatsappWebhookOrder
    {
        return $this->whatsappWebhookOrder;
    }

    public function setWhatsappWebhookOrder(?WhatsappWebhookOrder $whatsappWebhookOrder): void
    {
        $this->whatsappWebhookOrder = $whatsappWebhookOrder;
    }

    public function getWhatsappWebhookButton(): ?WhatsappWebhookButton
    {
        return $this->whatsappWebhookButton;
    }

    public function setWhatsappWebhookButton(?WhatsappWebhookButton $whatsappWebhookButton): void
    {
        $this->whatsappWebhookButton = $whatsappWebhookButton;
    }
}
