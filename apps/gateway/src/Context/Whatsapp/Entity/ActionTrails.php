<?php

namespace App\Context\Whatsapp\Entity;

use App\Context\Auth\Entity\Customer;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\CustomIdGenerator;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\UuidV7;

#[Table(name: 'actions_trails')]
#[Entity]
class ActionTrails
{
    #[Id]
    #[Column(type: UuidType::NAME, unique: true)]
    #[GeneratedValue(strategy: 'CUSTOM')]
    #[CustomIdGenerator('doctrine.uuid_generator')]
    private UuidV7 $id;

    #[Column(length: 255, nullable: true)]
    private ?string $status;

    #[Column(length: 255, nullable: true)]
    private ?string $context = null; // will be the data to pass

    #[Column(name: 'action', length: 255, nullable: true)]
    private ?string $action = null;

    #[Column(name: 'parent', length: 255, nullable: true)]
    private ?string $parent = null;

    #[Column]
    private \DateTimeImmutable $occuredAt;

    #[ManyToOne(targetEntity: Customer::class, inversedBy: 'actionTrails')]
    private Customer $customer;

    public function __construct()
    {
        $this->occuredAt = new \DateTimeImmutable();
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getId(): UuidV7
    {
        return $this->id;
    }

    public function getContext(): ?string
    {
        return $this->context;
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    public function getOccuredAt(): \DateTimeImmutable
    {
        return $this->occuredAt;
    }

    public function addNewCustomer(Customer $customer): void
    {
        $this->customer = $customer;
        $customer->addNewAction($this);
    }

    public function setId(UuidV7 $id): void
    {
        $this->id = $id;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function setContext(?string $context): void
    {
        $this->context = $context;
    }

    public function setAction(string $action): void
    {
        $this->action = $action;
    }

    public function setOccuredAt(\DateTimeImmutable $occuredAt): void
    {
        $this->occuredAt = $occuredAt;
    }

    public function getParent(): ?string
    {
        return $this->parent;
    }

    public function setParent(?string $parent): void
    {
        $this->parent = $parent;
    }

    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function setCustomer(Customer $customer): void
    {
        $this->customer = $customer;
    }
}
