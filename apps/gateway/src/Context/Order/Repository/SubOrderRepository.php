<?php

namespace App\Context\Order\Repository;

use App\Context\Auth\Entity\Supplier;
use App\Context\Order\Entity\SubOrder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class SubOrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubOrder::class);
    }

    public function getBySupplier(Supplier $supplier): array
    {
        return $this->findBy(['supplier' => $supplier->getId()]);
    }

    public function getSupplierOrders(Supplier $supplier, bool $fetchLastTen = false): array
    {
        $qb = $this->createQueryBuilder('o')
            ->andWhere('o.supplier = :supplierId')
            ->setParameter('supplierId', $supplier->getId()->toBinary())
            ->orderBy('o.orderedAt', 'ASC');

        if (true === $fetchLastTen) {
            $qb->setMaxResults(10);
        }

        return $qb->getQuery()->execute();
    }

    /**
     * @param array<string> $status
     *
     * @return array|int|object[]
     */
    public function getSuppliersOrderByStatus(Supplier $supplier, array $status, bool $getCount = false): array|int
    {
        $data = $this->findBy([
            'supplier' => $supplier->getId(),
            'status' => $status,
        ]);
        if (true === $getCount) {
            return count($data);
        }

        return $data;
    }
}
