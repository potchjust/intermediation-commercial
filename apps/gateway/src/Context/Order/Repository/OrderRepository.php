<?php

namespace App\Context\Order\Repository;

use App\Context\Auth\Entity\Customer;
use App\Context\Order\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function findByCustomer(Customer $customer)
    {
        return $this->findBy(['customer' => $customer]);
    }
}
