<?php

namespace App\Context\Order\Repository;

use App\Context\Auth\Entity\Customer;
use App\Context\Whatsapp\Entity\ActionTrails;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ActionTrailsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ActionTrails::class);
    }

    public function getTheLastestActionPerCustom(Customer $customer)
    {
        return $this->createQueryBuilder('actions')
             ->innerJoin('actions.customers', 'customers')
             ->andWhere('customers.id = :customer')
             ->setParameter('customer', $customer->getId())
             ->getQuery()->setMaxResults(1)
             ->getOneOrNullResult();
    }
}
