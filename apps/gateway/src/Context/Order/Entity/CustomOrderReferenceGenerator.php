<?php

namespace App\Context\Order\Entity;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Id\AbstractIdGenerator;

class CustomOrderReferenceGenerator extends AbstractIdGenerator
{
    public function generateId(EntityManagerInterface $em, $entity): mixed
    {
        assert($entity instanceof SubOrder);
        $lastOrder = $em->getRepository('App\Context\Order\Entity\SubOrder')->findOneBy(['supplier' => $entity->getSupplier()->getId()], ['id' => 'DESC']);
        if ($lastOrder) {
            assert($lastOrder instanceof SubOrder);
            $lastReference = $lastOrder->getOrderReference();
            // Extract the numeric part of the reference and increment it
            $numericPart = (int) substr($lastReference, 2);
            $nextReference = 'ORD-'.strtoupper($entity->getSupplier()->getSubdomain()).str_pad($numericPart + 1, strlen($lastReference) - 2, '0', STR_PAD_LEFT);
        } else {
            // If no previous orders exist, start with 'OR00001'
            $nextReference = 'ORD-'.strtoupper($entity->getSupplier()->getSubdomain()).'00001';
        }

        return $nextReference;
    }
}
