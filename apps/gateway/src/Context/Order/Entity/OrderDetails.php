<?php

namespace App\Context\Order\Entity;

use App\Context\Catalog\Entity\Product;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Symfony\Component\Uid\UuidV7;

#[ORM\Table(name: 'order_details')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class OrderDetails
{
    #[ORM\Id]
    #[Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator('doctrine.uuid_generator')]
    private UuidV7 $id;

    #[Column]
    private int $quantity;

    #[ORM\ManyToOne(targetEntity: Product::class, inversedBy: 'orderDetails')]
    private Product $product;

    #[Column(length: 255, nullable: true)]
    private ?string $status = null; // si la ligne a été validé oui ou non

    #[ORM\ManyToOne(targetEntity: SubOrder::class, inversedBy: 'orderDetails')]
    private SubOrder $subOrder;

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    public function getSubOrder(): SubOrder
    {
        return $this->subOrder;
    }

    public function setSubOrder(SubOrder $subOrder): void
    {
        $this->subOrder = $subOrder;
    }

    public function getId(): UuidV7
    {
        return $this->id;
    }

    public function setId(UuidV7 $id): void
    {
        $this->id = $id;
    }
    /*
        #[ORM\PreUpdate]
        public function preUpdate(PreUpdateEventArgs $event){
            // .... your pre-update logic here
            $changes = $event->getEntityChangeSet();
            $object = $event->getObject();
            $objectManager = $event->getObjectManager();
            foreach ($changes as $field => $change) {
                $oldValue = $change[0];
                $newValue = $change[1];
                $orderDetailsChanges = new OrderDetailsChanges();
                $orderDetailsChanges->setAction('update');
                $orderDetailsChanges->setOrderDetailsId($object->getId());
                $orderDetailsChanges->setField($field);
                $orderDetailsChanges->setOldValue($oldValue);
                $orderDetailsChanges->setNewValue($newValue);
                $objectManager->persist($orderDetailsChanges);
            }
            $objectManager->flush();
        }*/
}
