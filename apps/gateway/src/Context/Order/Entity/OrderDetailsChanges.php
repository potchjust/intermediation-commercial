<?php

namespace App\Context\Order\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Symfony\Component\Uid\UuidV7;

#[ORM\Table(name: 'order_details_changes')]
#[ORM\Entity]
class OrderDetailsChanges
{
    #[ORM\Id]
    #[Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator('doctrine.uuid_generator')]
    private UuidV7 $id;

    #[Column]
    private UuidV7 $orderDetails_id;

    #[Column(length: 255)]
    private string $oldValue;

    #[Column(length: 255)]
    private string $newValue;

    public function getOrderDetailsId(): UuidV7
    {
        return $this->orderDetails_id;
    }

    public function setOrderDetailsId(UuidV7 $orderDetails_id): void
    {
        $this->orderDetails_id = $orderDetails_id;
    }

    public function getId(): UuidV7
    {
        return $this->id;
    }

    public function setId(UuidV7 $id): void
    {
        $this->id = $id;
    }

    public function getOldValue(): string
    {
        return $this->oldValue;
    }

    public function setOldValue(string $oldValue): void
    {
        $this->oldValue = $oldValue;
    }

    public function getNewValue(): string
    {
        return $this->newValue;
    }

    public function setNewValue(string $newValue): void
    {
        $this->newValue = $newValue;
    }
}
