<?php

namespace App\Context\Order\Entity;

use App\Context\Auth\Entity\Customer;
use App\Context\Auth\Entity\User;
use App\Context\Invoice\Entity\Invoice;
use App\Context\Payment\Entity\AdminPayment;
use App\Context\Payment\Entity\Payment;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\OneToMany;
use Symfony\Component\Uid\UuidV7;

#[ORM\Table(name: 'orders')]
#[ORM\Entity]
class Order
{
    #[ORM\Id]
    #[Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator('doctrine.uuid_generator')]
    private UuidV7 $id;

    #[Column(type: Types::DATETIME_IMMUTABLE)]
    private \DateTimeImmutable $orderedAt;

    #[Column(length: 255)]
    private string $status; // si la commande a été validé ou pas

    /* one user can make one one multiple order */
    #[ORM\ManyToOne(targetEntity: Customer::class, inversedBy: 'orders')]
    private Customer $customer;

    #[OneToMany(targetEntity: SubOrder::class, mappedBy: 'order')]
    private Collection $subOrders;

    /* one order can have multiple invoice */

    #[OneToMany(targetEntity: Invoice::class, mappedBy: 'order')]
    private Collection $invoices;

    // private Collection $subOrders;

    #[Column(length: 255)]
    private string $orderReference; // si la commande a été validé ou pas

    // payment

    #[ORM\OneToOne(targetEntity: AdminPayment::class, mappedBy: 'order')]
    private AdminPayment $payment;

    public function __construct()
    {
        $this->orderedAt = new \DateTimeImmutable();
        $this->subOrders = new ArrayCollection();
        $this->invoices = new ArrayCollection();
    }

    public function getOrderedAt(): \DateTimeImmutable
    {
        return $this->orderedAt;
    }

    public function setOrderedAt(\DateTimeImmutable $orderedAt): void
    {
        $this->orderedAt = $orderedAt;
    }

    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function setCustomer(Customer $customer): void
    {
        $this->customer = $customer;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function getSubOrders(): Collection
    {
        return $this->subOrders;
    }

    public function addSubOrder(SubOrder $subOrder): void
    {
        if (!$this->subOrders->contains($subOrder)) {
            $this->subOrders->add($subOrder);
            $subOrder->setOrder($this);
        }
    }

    public function setSubOrders(Collection $subOrders): void
    {
        $this->subOrders = $subOrders;
    }

    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function setInvoices(Collection $invoices): void
    {
        $this->invoices = $invoices;
    }

    public function getId(): UuidV7
    {
        return $this->id;
    }

    public function setId(UuidV7 $id): void
    {
        $this->id = $id;
    }

    public function getOrderReference(): string
    {
        return $this->orderReference;
    }

    public function setOrderReference(string $orderReference): void
    {
        $this->orderReference = $orderReference;
    }

    public function linkOrderToSubOrder(SubOrder $subOrder): void
    {
        if (!$this->subOrders->contains($subOrder)) {
            $this->subOrders[] = $subOrder;
            $subOrder->setOrder($this);
        }
    }

    public function getPayment(): AdminPayment
    {
        return $this->payment;
    }

    public function setPayment(AdminPayment $payment): void
    {
        $this->payment = $payment;
    }
}
