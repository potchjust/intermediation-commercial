<?php

namespace App\Context\Order\Entity;

use App\Context\Auth\Entity\Supplier;
use App\Context\Invoice\Entity\Invoice;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Symfony\Component\Uid\UuidV7;

#[ORM\Table(name: 'sub_orders')]
#[ORM\Entity]
class SubOrder
{
    #[ORM\Id]
    #[Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator('doctrine.uuid_generator')]
    private UuidV7 $id;

    #[ORM\OneToMany(targetEntity: OrderDetails::class, mappedBy: 'subOrder')]
    private Collection $orderDetails;

    #[ORM\ManyToOne(targetEntity: Supplier::class, inversedBy: 'subOrders')]
    private Supplier $supplier;

    #[ORM\OneToMany(targetEntity: Invoice::class, mappedBy: 'subOrder')]
    private Collection $invoices;

    #[ORM\ManyToOne(targetEntity: Order::class, inversedBy: 'subOrders')]
    private Order $order;

    #[Column(length: 255)]
    private string $status; // si la commande a été validé ou pas

    #[Column(length: 255)]
    private string $orderReference; // si la commande a été validé ou pas

    #[Column(type: Types::DATETIME_IMMUTABLE)]
    private \DateTimeImmutable $orderedAt;

    public function __construct()
    {
        $this->invoices = new ArrayCollection();
        $this->orderDetails = new ArrayCollection();
        $this->orderedAt = new \DateTimeImmutable();
    }

    public function getSupplier(): Supplier
    {
        return $this->supplier;
    }

    public function setSupplier(Supplier $supplier): void
    {
        $this->supplier = $supplier;
    }

    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function setInvoices(Collection $invoices): void
    {
        $this->invoices = $invoices;
    }

    public function getOrderDetails(): Collection
    {
        return $this->orderDetails;
    }

    public function setOrderDetails(Collection $orderDetails): void
    {
        $this->orderDetails = $orderDetails;
    }

    public function addOrderDetails(OrderDetails $orderDetails): void
    {
        if (!$this->orderDetails->contains($orderDetails)) {
            $this->orderDetails->add($orderDetails);
            $orderDetails->setSubOrder($this);
        }
    }

    public function getId(): UuidV7
    {
        return $this->id;
    }

    public function setId(UuidV7 $id): void
    {
        $this->id = $id;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function getOrderReference(): string
    {
        return $this->orderReference;
    }

    public function setOrderReference(string $orderReference): void
    {
        $this->orderReference = $orderReference;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function setOrder(Order $order): void
    {
        $this->order = $order;
    }

    public function getOrderedAt(): \DateTimeImmutable
    {
        return $this->orderedAt;
    }

    public function setOrderedAt(\DateTimeImmutable $orderedAt): void
    {
        $this->orderedAt = $orderedAt;
    }
}
