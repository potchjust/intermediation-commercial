<?php

namespace App\Context\Order\Enums;

enum OrderStatus: string
{
    case NEW = 'NEW';
    case PROCESSING = 'En cours de traitement';
    case PENDING_CUSTOMER_VALIDATION = 'En attente de validation du client';
    case HAS_BEEN_VALIDATED_BY_CUSTOMER = 'Validée par le client,en attente de paiement';
    case HAS_BEEN_CANCELLED_BY_CUSTOMER = 'Annulée par le client';
    case PENDING_PAYMENT = 'En attente de paiement';
    case PAID = 'Commande payée , en attente de livraison';
    case DELIVERED = 'Commande livrée';
}
