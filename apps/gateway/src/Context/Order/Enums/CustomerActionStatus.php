<?php

namespace App\Context\Order\Enums;

enum CustomerActionStatus: string
{
    case PROCESSING = 'PROCESSING';
    case END = 'END';
}
