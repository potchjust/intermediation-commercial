<?php

namespace App\Context\Order\Enums;

enum OrderDetailsStatus: string
{
    case PENDING = 'En attente de traitement ou de validation';
    case VALIDATED = 'Ligne validée';
    case CHANGED = 'Produit non disponible: Equivalent proposé';
    case NOT_AVAILABLE = 'Produit non disponible';
    case HAS_BEEN_CANCELLED_BY_CUSTOMER = 'Annulée par le client';

    public static function getValuefromName(string $name): string
    {
        foreach (self::cases() as $status) {
            if ($name === $status->name) {
                return $status->value;
            }
        }
        throw new \ValueError("$name is not a valid backing value for enum ".self::class);
    }
}
