<?php

namespace App\Context\Order\Enums;

enum SubOrderStatus: string
{
    case NEW = 'NEW';
    case PROCESSING = 'En cours de traitement';
    case VALIDATED = 'Commande validée';
    case HAS_BEEN_CANCELLED_BY_CUSTOMER = 'Annulée par le client';

    public static function getValuefromName(string $name): string
    {
        foreach (self::cases() as $status) {
            if ($name === $status->name) {
                return $status->value;
            }
        }
        throw new \ValueError("$name is not a valid backing value for enum ".self::class);
    }
}
