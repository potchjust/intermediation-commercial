<?php

namespace App\Context\Order\UseCase;

use App\Context\Order\Entity\Order;
use App\Context\Order\Entity\SubOrder;
use App\Context\Order\Enums\OrderStatus;
use App\Context\Order\Enums\SubOrderStatus;
use App\Context\Order\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;

class UpdateOrderStatus
{
    public function __construct(private readonly OrderRepository $orderRepository, private EntityManagerInterface $entityManager)
    {
    }

    public function updateOrders(): void
    {
        // get all the orders
        $orders = $this->orderRepository->findAll();
        foreach ($orders as $order) {
            assert($order instanceof Order);
            // get all the subOrderd
            $subOrders = $order->getSubOrders();
            $hasAllSubOrderBeenValidated = array_filter($subOrders->toArray(), function (SubOrder $subOrder) {
                assert($subOrder instanceof SubOrder);

                return SubOrderStatus::VALIDATED === $subOrder->getStatus();
            });
            if (count($hasAllSubOrderBeenValidated) === $subOrders->count() && $order->getStatus() === OrderStatus::PROCESSING->name) {
                $order->setStatus(OrderStatus::PENDING_CUSTOMER_VALIDATION->name);
                $this->entityManager->persist($order);
            }
        }
        $this->entityManager->flush();
    }
}
