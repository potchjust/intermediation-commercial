<?php

namespace App\Context\Order\UseCase;

use App\Context\Auth\Entity\Customer;
use App\Context\Order\Entity\Order;
use App\Context\Order\Enums\OrderStatus;
use App\Context\Order\Repository\OrderRepository;
use App\Context\Whatsapp\Dto\Messages\Text\WhatsappTextMessage;
use App\Context\Whatsapp\Enums\WhatsappMessageType;
use App\Context\Whatsapp\UseCase\Request\SendProcessingMessage;
use App\Context\Whatsapp\UseCase\Request\SendTemplateMessage;
use App\Context\Whatsapp\UseCase\Whatsapp\Messages\SendMessageRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;

final class ValidateCustomerOrder
{
    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly OrderRepository $orderRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly SendProcessingMessage $sendProcessingMessage,
        private readonly SendTemplateMessage $sendTemplateMessage, )
    {
    }

    public function validate(string $orderReference, Customer $customer)
    {
        $order = $this->orderRepository->findOneBy([
            'orderReference' => $orderReference,
        ]);
        if (!$order) {
            $message = 'La commande avec cette reference n\'existe pas. Veuillez reessayer ';

            return $this->sendOrderNotFoundMessage($customer, $message);
        }

        assert($order instanceof Order);
        if ($order->getStatus() === OrderStatus::HAS_BEEN_CANCELLED_BY_CUSTOMER->name) {
            $message = 'Erreur lors de la validation de la commande: Cette commande a déjà été annulée';

            return $this->sendOrderNotFoundMessage($customer, $message);
        }

        if ($order->getStatus() === OrderStatus::HAS_BEEN_VALIDATED_BY_CUSTOMER->name) {
            $message = 'Erreur lors de la validation de la commande: Cette commande a déjà été validée';

            return $this->sendOrderNotFoundMessage($customer, $message);
        }

        if ($order->getStatus() === OrderStatus::PAID->name) {
            $message = 'Erreur lors de la validation de la commande: Cette commande a déjà été payé';

            return $this->sendOrderNotFoundMessage($customer, $message);
        }

        if ($order->getStatus() === OrderStatus::DELIVERED->name) {
            $message = 'Erreur lors de la validation de la commande: Cette commande a déjà été livrée';

            return $this->sendOrderNotFoundMessage($customer, $message);
        }

        if ($order->getStatus() === OrderStatus::NEW->name || $order->getStatus() === OrderStatus::PROCESSING->name) {
            $message = 'Cette commande ne peut être validé car elle est en cours de traitement';

            return $this->sendOrderNotFoundMessage($customer, $message);
        }

        $this->sendProcessingMessage->send($customer);
        $order->setStatus(OrderStatus::HAS_BEEN_VALIDATED_BY_CUSTOMER->name);
        $this->entityManager->persist($order);
        $this->entityManager->flush();

        return [$this->sendConfirmationMessage($order->getCustomer())];
    }

    private function sendConfirmationMessage(Customer $customer): SendMessageRequest
    {
        return $this->sendTemplateMessage->createRequest(
            customer: $customer,
            templateName: 'order_confirmation_message'
        );
    }

    private function sendOrderNotFoundMessage(Customer $customer, string $message): SendMessageRequest
    {
        $textMessage = new WhatsappTextMessage();
        $textMessage->setMessagingProduct('whatsapp');
        $textMessage->setTo($customer->getPhoneNumber());
        $textMessage->setType(WhatsappMessageType::TEXT->value);
        $formattedMessage = $message;
        $textMessage->setContent([
            'preview_url' => false,
            'body' => $formattedMessage,
        ]);
        $request = new SendMessageRequest($this->serializer);
        $request->setBody($textMessage);

        return $request;
    }
}
