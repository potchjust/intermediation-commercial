<?php

namespace App\Context\Order\UseCase;

use App\Context\Auth\Entity\Customer;
use App\Context\Order\Entity\Order;
use App\Context\Order\Entity\OrderDetails;
use App\Context\Order\Entity\SubOrder;
use App\Context\Order\Enums\OrderDetailsStatus;
use App\Context\Order\Repository\OrderRepository;
use App\Context\Whatsapp\Dto\Messages\Template\WhatsappMessageTemplateComponents;
use App\Context\Whatsapp\Dto\Messages\Template\WhatsappMessageTemplateComponentsParameters;
use App\Context\Whatsapp\Dto\Messages\Text\WhatsappTextMessage;
use App\Context\Whatsapp\Enums\WhatsappMessageComponentsSubType;
use App\Context\Whatsapp\Enums\WhatsappMessageComponentsType;
use App\Context\Whatsapp\Enums\WhatsappMessageParametersType;
use App\Context\Whatsapp\Enums\WhatsappMessageType;
use App\Context\Whatsapp\UseCase\Request\SendTemplateMessage;
use App\Context\Whatsapp\UseCase\Whatsapp\Messages\SendMessageRequest;
use Symfony\Component\Serializer\SerializerInterface;

class GetSingleOrderDetails
{
    public function __construct(private readonly OrderRepository $orderRepository,
        private SerializerInterface $serializer,
        private SendTemplateMessage $sendTemplateMessage)
    {
    }

    public function handleSingleOrderReference(string $orderReference): SendMessageRequest
    {
        $order = $this->orderRepository->findOneBy([
            'orderReference' => $orderReference,
        ]);

        return $this->displaySingleOrder($order);
    }

    private function displaySingleOrder(Order $order): SendMessageRequest
    {
        // Début de la table
        $orderTable = '| Nom du Produit | Quantité | Prix| Etat| \\n';
        $orderTable .= '| -------------- | -------- | ------ || ------ | \\n';
        $totalPrice = 0;
        foreach ($order->getSubOrders() as $subOrder) {
            assert($subOrder instanceof SubOrder);
            foreach ($subOrder->getOrderDetails() as $orderItem) {
                $status = match ($orderItem->getStatus()) {
                    OrderDetailsStatus::VALIDATED->name => '✅',
                    OrderDetailsStatus::NOT_AVAILABLE->name => '❌',
                    OrderDetailsStatus::PENDING->name => '⏳',
                    OrderDetailsStatus::CHANGED->name => '🔁',
                    OrderDetailsStatus::HAS_BEEN_CANCELLED_BY_CUSTOMER->name => '❌👨🏿',
                };
                assert($orderItem instanceof OrderDetails);
                // $price = 0;
                $price = $orderItem->getProduct()->getPrice() * $orderItem->getQuantity();
                $totalPrice += $price;
                $orderTable .= '| '.$orderItem->getProduct()->getLabel().' | '.strval($orderItem->getQuantity()).' | '.strval($price).'|'.$status.' |\\n';
            }
        }

        $orderTableComponentParameter = new WhatsappMessageTemplateComponentsParameters();
        $orderTableComponentParameter->setType(WhatsappMessageParametersType::TEXT->value);
        $orderTableComponentParameter->setText($orderTable);
        // order
        $orderedAtComponentParameter = new WhatsappMessageTemplateComponentsParameters();
        $orderedAtComponentParameter->setType(WhatsappMessageParametersType::TEXT->value);
        $orderedAtComponentParameter->setText($order->getOrderedAt()->format('Y-m-d H:i:s'));
        // phoneNumber
        $phoneNumberComponentParameter = new WhatsappMessageTemplateComponentsParameters();
        $phoneNumberComponentParameter->setType(WhatsappMessageParametersType::TEXT->value);
        $phoneNumberComponentParameter->setText($order->getCustomer()->getPhoneNumber());

        // order reference
        $orderReferenceComponentParameter = new WhatsappMessageTemplateComponentsParameters();
        $orderReferenceComponentParameter->setType(WhatsappMessageParametersType::TEXT->value);
        $orderReferenceComponentParameter->setText($order->getOrderReference());

        $priceReferenceComponentParameter = new WhatsappMessageTemplateComponentsParameters();
        $priceReferenceComponentParameter->setType(WhatsappMessageParametersType::TEXT->value);

        $priceReferenceComponentParameter->setText(strval($totalPrice));

        // add the buttons
        $cancelButtonComponent = new WhatsappMessageTemplateComponents();
        $cancelButtonComponent->setType(WhatsappMessageComponentsType::BUTTON->value);
        $cancelButtonComponent->setIndex(0);
        $cancelButtonComponent->setSubType(WhatsappMessageComponentsSubType::QUICK_REPLY->value);
        $cancelButtonComponentParameter = new WhatsappMessageTemplateComponentsParameters();
        $cancelButtonComponentParameter->setType(WhatsappMessageParametersType::PAYLOAD->value);
        $cancelButtonComponentParameter->setText('CANCEL_ORDER');
        $cancelButtonComponentParameter->setAction(['payload' => $order->getOrderReference()]);
        $cancelButtonComponent->addParameter($cancelButtonComponentParameter);

        $validateButtonComponent = new WhatsappMessageTemplateComponents();
        $validateButtonComponent->setType(WhatsappMessageParametersType::BUTTON->value);
        $validateButtonComponent->setSubType(WhatsappMessageComponentsSubType::QUICK_REPLY->value);
        $validateButtonComponent->setIndex(1);
        // add the buttons
        $validateButtonComponentParameter = new WhatsappMessageTemplateComponentsParameters();
        $validateButtonComponentParameter->setText('VALIDATE_ORDER');
        $validateButtonComponentParameter->setType(WhatsappMessageParametersType::PAYLOAD->value);
        $validateButtonComponentParameter->setAction(['payload' => $order->getOrderReference()]);
        $validateButtonComponent->addParameter($validateButtonComponentParameter);
        // add the buttons
        $paymentButtonComponent = new WhatsappMessageTemplateComponents();
        $paymentButtonComponent->setType(WhatsappMessageParametersType::BUTTON->value);
        $paymentButtonComponent->setSubType(WhatsappMessageComponentsSubType::QUICK_REPLY->value);
        $paymentButtonComponent->setIndex(1);
        $paymentButtonComponentParameter = new WhatsappMessageTemplateComponentsParameters();
        $paymentButtonComponentParameter->setText('PAY_ORDER');
        $paymentButtonComponentParameter->setType(WhatsappMessageParametersType::PAYLOAD->value);
        $paymentButtonComponentParameter->setAction(['payload' => $order->getOrderReference()]);
        $paymentButtonComponent->addParameter($paymentButtonComponentParameter);

        return $this->sendTemplateMessage->createRequest(
            customer: $order->getCustomer(),
            templateName: 'single_orders_details_v2',
            bodyParameters: [
                $orderTableComponentParameter,
                $orderedAtComponentParameter,
                $phoneNumberComponentParameter,
                $orderReferenceComponentParameter,
                $priceReferenceComponentParameter,
            ], components: [
                $cancelButtonComponent,
                $validateButtonComponent,
                $paymentButtonComponent,
            ]
        );
    }

    public function askUserToEnterTheReference(Customer $customer): SendMessageRequest
    {
        $textMessage = new WhatsappTextMessage();
        $textMessage->setMessagingProduct('whatsapp');
        $textMessage->setTo($customer->getPhoneNumber());
        $textMessage->setType(WhatsappMessageType::TEXT->value);
        $formattedMessage = '
            Veuillez indiquer la reference de la commande : Elle commence par Ref-ORD-XXXX
            Si vous souhaitez annuler le processus, saisissez le mot Annulez';
        $textMessage->setContent([
            'preview_url' => false,
            'body' => $formattedMessage,
        ]);
        $request = new SendMessageRequest($this->serializer);
        $request->setBody($textMessage);

        return $request;
    }
}
