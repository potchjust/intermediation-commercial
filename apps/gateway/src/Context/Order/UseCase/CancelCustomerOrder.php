<?php

namespace App\Context\Order\UseCase;

use App\Context\Auth\Entity\Customer;
use App\Context\Order\Entity\Order;
use App\Context\Order\Entity\SubOrder;
use App\Context\Order\Enums\OrderDetailsStatus;
use App\Context\Order\Enums\OrderStatus;
use App\Context\Order\Enums\SubOrderStatus;
use App\Context\Order\Repository\OrderRepository;
use App\Context\Whatsapp\Dto\Messages\Template\WhatsappMessageTemplateComponentsParameters;
use App\Context\Whatsapp\Dto\Messages\Text\WhatsappTextMessage;
use App\Context\Whatsapp\Enums\WhatsappMessageParametersType;
use App\Context\Whatsapp\Enums\WhatsappMessageType;
use App\Context\Whatsapp\UseCase\Request\SendProcessingMessage;
use App\Context\Whatsapp\UseCase\Request\SendTemplateMessage;
use App\Context\Whatsapp\UseCase\Whatsapp\Messages\SendMessageRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class CancelCustomerOrder
{
    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly OrderRepository $orderRepository,
        private readonly SendProcessingMessage $sendProcessingMessage,
        private readonly SendTemplateMessage $sendTemplateMessage,
        private readonly EntityManagerInterface $entityManager)
    {
    }

    public function cancel(string $orderReference, Customer $customer)
    {
        $order = $this->orderRepository->findOneBy([
            'orderReference' => trim($orderReference),
        ]);
        $this->sendProcessingMessage->send($customer);
        if (!$order) {
            $message = 'La commande avec cette reference n\'existe pas. Veuillez reessayer ';

            return $this->sendOrderNotFoundMessage($customer, $message);
        }

        assert($order instanceof Order);
        if ($order->getStatus() === OrderStatus::HAS_BEEN_VALIDATED_BY_CUSTOMER->name) {
            $message = 'Erreur cette commande ne peux plus être annulée car elle a déjà été validé';

            return $this->sendOrderNotFoundMessage($customer, $message);
        }

        if ($order->getStatus() === OrderStatus::PAID->name) {
            $message = 'Erreur cette commande ne peux plus être annulée car elle a déjà été payé';

            return $this->sendOrderNotFoundMessage($customer, $message);
        }
        $order->setStatus(OrderStatus::HAS_BEEN_CANCELLED_BY_CUSTOMER->name);
        foreach ($order->getSubOrders() as $subOrder) {
            $subOrder->setStatus(SubOrderStatus::HAS_BEEN_CANCELLED_BY_CUSTOMER->name);
            assert($subOrder instanceof SubOrder);
            foreach ($subOrder->getOrderDetails() as $orderDetail) {
                $orderDetail->setStatus(OrderDetailsStatus::HAS_BEEN_CANCELLED_BY_CUSTOMER->name);
            }
        }
        $this->entityManager->persist($order);
        $this->entityManager->flush();

        return $this->sendOrderCancelationMessage($order->getCustomer(), $order);
    }

    private function sendOrderCancelationMessage(Customer $customer, Order $order): SendMessageRequest
    {
        $orderReferenceComponentParameter = new WhatsappMessageTemplateComponentsParameters();
        $orderReferenceComponentParameter->setType(WhatsappMessageParametersType::TEXT->value);
        $orderReferenceComponentParameter->setText($order->getOrderReference());
        // order
        $orderedAtComponentParameter = new WhatsappMessageTemplateComponentsParameters();
        $orderedAtComponentParameter->setType(WhatsappMessageParametersType::TEXT->value);
        $orderedAtComponentParameter->setText($order->getOrderedAt()->format('Y-m-d H:i:s'));

        return $this->sendTemplateMessage->createRequest(
            customer: $customer,
            templateName: 'order_cancellation_message',
            bodyParameters: [
                $orderedAtComponentParameter,
                $orderReferenceComponentParameter,
            ]
        );
    }

    private function sendOrderNotFoundMessage(Customer $customer, string $message): SendMessageRequest
    {
        $textMessage = new WhatsappTextMessage();
        $textMessage->setMessagingProduct('whatsapp');
        $textMessage->setTo($customer->getPhoneNumber());
        $textMessage->setType(WhatsappMessageType::TEXT->value);
        $formattedMessage = $message;
        $textMessage->setContent([
            'preview_url' => false,
            'body' => $formattedMessage,
        ]);
        $request = new SendMessageRequest($this->serializer);
        $request->setBody($textMessage);

        return $request;
    }
}
