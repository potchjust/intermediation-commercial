<?php

namespace App\Context\Order\UseCase;

use App\Context\Order\Entity\SubOrder;
use App\Context\Order\Enums\SubOrderStatus;
use Doctrine\ORM\EntityManagerInterface;

class TerminateOrderChanges
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function terminate(SubOrder $subOrder): void
    {
        $subOrder->setStatus(SubOrderStatus::VALIDATED->name);
        $this->entityManager->persist($subOrder);
        $this->entityManager->flush();
    }
}
