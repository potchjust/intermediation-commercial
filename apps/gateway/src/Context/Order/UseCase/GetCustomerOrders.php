<?php

namespace App\Context\Order\UseCase;

use App\Context\Auth\Entity\Customer;
use App\Context\Order\Entity\Order;
use App\Context\Order\Enums\OrderStatus;
use App\Context\Order\Repository\OrderRepository;
use App\Context\Whatsapp\Dto\Messages\Template\WhatsappMessageTemplateComponentsParameters;
use App\Context\Whatsapp\Enums\WhatsappMessageParametersType;
use App\Context\Whatsapp\UseCase\Request\SendTemplateMessage;

final readonly class GetCustomerOrders
{
    public function __construct(private OrderRepository $orderRepository,
        private SendTemplateMessage $sendTemplateMessage)
    {
    }

    public function get(Customer $customer)
    {
        $orders = $this->orderRepository->findByCustomer($customer);
        // Début de la table
        $orderTable = '| Référence| Date de la commande | Etat| \\n';
        $orderTable .= '| -------------- | -------- | ------ |\\n';
        if (0 === count($orders)) {
            $orderTable .= 'Vous n\'avez pas encore de commande pour cette client';
        } else {
            foreach ($orders as $order) {
                assert($order instanceof Order);
                $status = match ($order->getStatus()) {
                    OrderStatus::NEW->name => '🆕',
                    OrderStatus::PROCESSING->name => '⏳',
                    OrderStatus::PENDING_CUSTOMER_VALIDATION->name => 'ℹ️👨🏿',
                    OrderStatus::HAS_BEEN_VALIDATED_BY_CUSTOMER->name => '👨✅',
                    OrderStatus::HAS_BEEN_CANCELLED_BY_CUSTOMER->name => '👨❌',
                    OrderStatus::PENDING_PAYMENT->name => '⏳💵',
                    OrderStatus::PAID->name => '💵',
                    OrderStatus::DELIVERED->name => '📦',
                };
                $orderTable .= '|📦 '.$order->getOrderReference().' |💸 '.$order->getOrderedAt()->format('D, d M Y H:i:s').' |ℹ️ '.$status.' |\\n';
                $orderTable .= '| -------------- | -------- | ------ |\\n';
            }
        }

        $orderTableComponentParameter = new WhatsappMessageTemplateComponentsParameters();
        $orderTableComponentParameter->setType(WhatsappMessageParametersType::TEXT->value);
        $orderTableComponentParameter->setText($orderTable);

        return $this->sendTemplateMessage->createRequest(
            customer: $customer,
            templateName: 'list_orders',
            bodyParameters: [
                $orderTableComponentParameter,
            ]
        );
    }
}
