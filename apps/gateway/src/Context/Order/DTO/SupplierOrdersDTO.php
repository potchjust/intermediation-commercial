<?php

namespace App\Context\Order\DTO;

use App\Context\Auth\Entity\Supplier;
use App\Context\Catalog\Repository\ProductRepository;
use App\Context\Order\Entity\SubOrder;
use App\Context\Order\Enums\SubOrderStatus;
use Symfony\Component\Uid\UuidV7;

class SupplierOrdersDTO
{
    private UuidV7 $id;
    private string $orderReference;
    private array $status;
    private string $orderedAt;
    private array $details;
    private Supplier $supplier;
    private float $orderTotal = 0;
    private string $clientName;

    public function __construct(SubOrder $subOrder, ProductRepository $productRepository)
    {
        $this->id = $subOrder->getId();
        $this->orderReference = $subOrder->getOrderReference();

        $this->status =  [
            'value' => SubOrderStatus::getValuefromName($subOrder->getStatus()),
            'name'=> $subOrder->getStatus()
        ];
        switch ($subOrder->getStatus()) {
            case SubOrderStatus::PROCESSING->name:
                $this->status['color'] = 'yellow';
                break;
            case SubOrderStatus::VALIDATED->name:
                $this->status['color'] = 'green';
                break;
            case SubOrderStatus::HAS_BEEN_CANCELLED_BY_CUSTOMER->name:
                $this->status['color'] = 'red';
                break;
        }

        $this->orderedAt = $subOrder->getOrder()->getOrderedAt()->format('r');
        $this->supplier = $subOrder->getSupplier();
        $currentOrderProducts = [];
        $this->clientName = 'Etoile Du Matin';

        if ($subOrder->getOrderDetails()->count() > 0) {
            foreach ($subOrder->getOrderDetails() as $orderDetail) {
                $details = new OrderDetailsDTO($orderDetail);
                $this->orderTotal = $this->orderTotal + $details->getSubTotal();
                $currentOrderProducts[] = $orderDetail->getProduct();
                $orderProductAlternatives = $productRepository->getOrderProductsAlternatives($currentOrderProducts);
                $details->setAlternatives($orderProductAlternatives);
                $this->details[] = $details;
            }
        }
    }

    public function getId(): UuidV7
    {
        return $this->id;
    }

    public function getOrderReference(): string
    {
        return $this->orderReference;
    }


    public function getOrderedAt(): string
    {
        return $this->orderedAt;
    }

    public function getSupplier(): Supplier
    {
        return $this->supplier;
    }

    public function getDetails(): array
    {
        return $this->details ?? [];
    }

    public function getOrderTotal(): float
    {
        return $this->orderTotal;
    }

    public function getStatus(): array
    {
        return $this->status;
    }

    public function setStatus(array $status): void
    {
        $this->status = $status;
    }

    public function getClientName(): string
    {
        return $this->clientName;
    }

    public function setClientName(string $clientName): void
    {
        $this->clientName = $clientName;
    }
}
