<?php

namespace App\Context\Order\DTO;

use App\Context\Catalog\Entity\Product;
use App\Context\Order\Entity\OrderDetails;
use App\Context\Order\Enums\OrderDetailsStatus;
use App\Context\Order\Enums\SubOrderStatus;
use Symfony\Component\Uid\UuidV7;

class OrderDetailsDTO
{
    private UuidV7 $id;

    // private UuidV7 $productId;
    private int $quantity;
    private string $unitPrice;
    private array $status;
    private string $productName;
    private float $subTotal;
    private bool $isInStock;
    private array $alternatives;
    private Product $product;
    public function getAlternatives(): array
    {
        return $this->alternatives;
    }

    public function setAlternatives(array $alternatives): void
    {
        $this->alternatives = $alternatives;
    }

    public function __construct(OrderDetails $orderDetails)
    {
        $this->status =  [
            'value' => OrderDetailsStatus::getValuefromName($orderDetails->getStatus()),
            'name'=>  $orderDetails->getStatus()
        ];
        switch ( $orderDetails->getStatus()) {
            case OrderDetailsStatus::PENDING->name:
                $this->status['color'] = 'sky';
                break;
            case SubOrderStatus::VALIDATED->name:
                $this->status['color'] = 'green';
                break;
            case OrderDetailsStatus::CHANGED->name:
                $this->status['color'] = 'orange';
                break;
            case OrderDetailsStatus::NOT_AVAILABLE->name:
                $this->status['color'] = 'yellow';
                break;
            case SubOrderStatus::HAS_BEEN_CANCELLED_BY_CUSTOMER->name:
                $this->status['color'] = 'red';
                break;
        }
        $this->id = $orderDetails->getId();
        $this->quantity = $orderDetails->getQuantity();
        $this->productName = $orderDetails->getProduct()->getLabel();
        $this->unitPrice = $orderDetails->getProduct()->getPrice();
        $this->subTotal = $orderDetails->getQuantity() * $orderDetails->getProduct()->getPrice();
        $this->alternatives = [];
        $this->product = $orderDetails->getProduct();
        // propose alternative
        if ($orderDetails->getProduct()->getCount() > $orderDetails->getQuantity()) {
            // if the product is not in suffisient stock we get some product that are in the same category
            $this->setIsInStock(true);
        } else {
            $this->setIsInStock(false);
        }
    }

    public function getStatus(): array
    {
        return $this->status;
    }

    public function getProductName(): string
    {
        return $this->productName;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getUnitPrice(): string
    {
        return $this->unitPrice;
    }

    public function getSubTotal(): float
    {
        return $this->subTotal;
    }

    public function getIsInStock(): bool
    {
        return $this->isInStock;
    }

    public function getId(): UuidV7
    {
        return $this->id;
    }

    public function setId(UuidV7 $id): void
    {
        $this->id = $id;
    }

    public function setIsInStock(bool $isInStock): void
    {
        $this->isInStock = $isInStock;
    }

    public function getAlternativesItems()
    {
        $items = [];
        foreach ($this->getAlternatives() as $product) {
            $items[] = [
                'id' => $product->getId(),
                'label' => $product->getLabel(),
            ];
        }

        return $items;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }
}
