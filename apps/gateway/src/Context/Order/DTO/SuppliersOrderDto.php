<?php

namespace App\Context\Order\DTO;

use App\Context\Auth\Entity\Supplier;
use App\Context\Order\Entity\SubOrder;
use Symfony\Component\Uid\UuidV7;

class SuppliersOrderDto
{
    private UuidV7 $id;
    private string $orderReference;
    private string $status;
    private string $orderedAt;
    private array $details;
    private Supplier $supplier;
    private float $orderTotal = 0;

    public function __construct(SubOrder $subOrder, bool $flat = false)
    {
        $this->id = $subOrder->getId();
        $this->orderReference = $subOrder->getOrderReference();
        $this->status = $subOrder->getStatus();
        $this->orderedAt = $subOrder->getOrder()->getOrderedAt()->format('Y-m-d H:i:s');
        $this->supplier = $subOrder->getSupplier();
        if ($flat) {
            if ($subOrder->getOrderDetails()->count() > 0) {
                foreach ($subOrder->getOrderDetails() as $orderDetail) {
                    $details = new OrderDetailsDTO($orderDetail);
                    $this->orderTotal = $this->orderTotal + $details->getSubTotal();
                    $this->details[] = $details;
                }
            }
        }
    }

    public function getId(): UuidV7
    {
        return $this->id;
    }

    public function getOrderReference(): string
    {
        return $this->orderReference;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getOrderedAt(): string
    {
        return $this->orderedAt;
    }

    public function getSupplier(): Supplier
    {
        return $this->supplier;
    }

    public function getDetails(): array
    {
        return $this->details ?? [];
    }

    public function getOrderTotal(): float
    {
        return $this->orderTotal;
    }
}
