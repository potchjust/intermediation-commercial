<?php

namespace App\Context\Wordpress\Woocommerce\UseCase;

use App\Context\Catalog\Entity\Category;
use App\Context\Catalog\Entity\Product;
use App\Context\Catalog\Repository\ProductRepository;
use App\Infrastructure\Wordpress\Woocommerce\Request\WoocommerceSendPostRequest;
use App\Infrastructure\Wordpress\Woocommerce\WooCommerceApi;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class CreateProductsOnWoocommerce
{
    public function __construct(private ProductRepository $productRepository, private WooCommerceApi $wooCommerceApi, private EntityManagerInterface $entityManager)
    {
    }

    public function create(SymfonyStyle $symfonyStyle): void
    {
        $symfonyStyle->title('Creation des produits dans la base de données woocommerce');
        $products = $this->productRepository->findAll();
        // create the categories in Woocommerce
        $createProductsRequest = new WoocommerceSendPostRequest('products');
        foreach ($symfonyStyle->progressIterate($products) as $product) {
            assert($product instanceof Product);
            if (null === $product->getWoocommerceId()) {
                $symfonyStyle->info("Creation du produit {$product->getLabel()} ");
                $requestItem['name'] = $product->getLabel();
                $requestItem['description'] = $product->getDescription();
                $requestItem['regular_price'] = strval($product->getPrice());
                $requestItem['type'] = 'simple';
                $requestItem['sku'] = $product->getId()->toRfc4122();
                $categories = [];
                // set the categories
                foreach ($product->getCategories() as $category) {
                    assert($category instanceof Category);
                    // faire une recherche par nom
                    $categories[] = [
                        'id' => $category->getWoocommerceId(),
                    ];
                }
                $requestItem['categories'] = $categories;
                $createProductsRequest->addParameter($requestItem);
                $response = json_decode($this->wooCommerceApi->send($createProductsRequest), true);
                $symfonyStyle->info("Creation du produit {$product->getLabel()} terminée ");
                $symfonyStyle->text("Mise à jour du produit {$product->getLabel()} avec l'id woocommerce ");
                $product->setWoocommerceId($response['id']);
                $this->entityManager->persist($product);
                $createProductsRequest->flushParameters();
                $symfonyStyle->info("Mise à jour du produit {$product->getLabel()} avec l'id woocommerce terminée ");
            } else {
                $symfonyStyle->warning("Ce produit existe déjà {$product->getLabel()} ");
            }
        }
        $this->entityManager->flush();
    }
}
