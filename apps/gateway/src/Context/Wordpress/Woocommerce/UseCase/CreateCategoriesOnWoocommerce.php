<?php

namespace App\Context\Wordpress\Woocommerce\UseCase;

use App\Context\Catalog\Entity\Category;
use App\Context\Catalog\Repository\CategoryRepository;
use App\Infrastructure\Wordpress\Woocommerce\Request\WoocommerceSendPostRequest;
use App\Infrastructure\Wordpress\Woocommerce\WooCommerceApi;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class CreateCategoriesOnWoocommerce
{
    public function __construct(
        private readonly CategoryRepository $categoryRepository,
        private readonly WooCommerceApi $wooCommerceApi,
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    public function create(SymfonyStyle $symfonyStyle): void
    {
        $symfonyStyle->title('Creation des catégories dans la base de données woocommerce');
        $categories = $this->categoryRepository->findAll();
        $createCategoriesRequest = new WoocommerceSendPostRequest('products/categories');
        foreach ($symfonyStyle->progressIterate($categories) as $category) {
            assert($category instanceof Category);
            $symfonyStyle->text("Creation de la categorie {$category->getLabel()} ");
            if (null !== $category->getWoocommerceId()) {
                $symfonyStyle->text("Cette categorie existe déjà sur woocommerce {$category->getLabel()} ");
            } else {
                $symfonyStyle->text("Creation de la categorie {$category->getLabel()} ");
                $productCategory = [];
                $productCategory['slug'] = $category->getId()->toRfc4122();
                $productCategory['name'] = $category->getLabel();
                $createCategoriesRequest->addParameter($productCategory);
                $response = json_decode($this->wooCommerceApi->send($createCategoriesRequest), true);
                $symfonyStyle->info("Creation de la categorie {$category->getLabel()} sur woocommerce terminée");
                $category->setWoocommerceId($response['id']);
                $this->entityManager->persist($category);
                $createCategoriesRequest->flushParameters();
                $symfonyStyle->info("Mise à jour de la categorie {$category->getLabel()} dans la base de donnée terminée");
            }
        }
        $this->entityManager->flush();
        $symfonyStyle->info('Creation des catagories dans la base de données woocommerce terminée');
        // create the categories in Woocommerce
        /* $createCategoriesRequest = new WoocommerceSendPostRequest('products/categories');
         foreach ($categories as $category) {
             assert($category instanceof Category);
             $productCategory = [];
             $productCategory['slug'] = $category->getId()->toRfc4122();
             $productCategory['name'] = $category->getLabel();
             $createCategoriesRequest->addParameter($productCategory);
             $response = json_decode($this->wooCommerceApi->send($createCategoriesRequest), true);
             $category->setWoocommerceId($response['id']);
             $this->entityManager->persist($category);
             $createCategoriesRequest->flushParameters();
         }
         $this->entityManager->flush();*/
    }
}
