<?php

namespace App\Context\Payment\Repository;

use App\Context\Auth\Entity\Supplier;
use App\Context\Payment\Entity\SupplierPayment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class SupplierPaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SupplierPayment::class);
    }

    public function getBySupplier(Supplier $supplier): array
    {
        return $this->findBy(['supplier' => $supplier->getId()]);
    }
}
