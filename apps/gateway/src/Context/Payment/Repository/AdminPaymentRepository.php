<?php

namespace App\Context\Payment\Repository;

use App\Context\Auth\Entity\Admin;
use App\Context\Payment\Entity\AdminPayment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AdminPaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdminPayment::class);
    }

    // Should be usefull when multiple tenant will be ok
    public function getByAdmin(Admin $admin): array
    {
        return $this->findBy(['admin' => $admin->getId()]);
    }
}
