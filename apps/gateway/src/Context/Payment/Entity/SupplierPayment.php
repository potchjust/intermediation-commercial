<?php

namespace App\Context\Payment\Entity;

use App\Context\Auth\Entity\Supplier;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'suppliers_payments')]
#[ORM\Entity]
class SupplierPayment extends Payment
{
    #[ORM\ManyToOne(targetEntity: Supplier::class, inversedBy: 'payments')]
    private Supplier $supplier;

    #[ORM\ManyToOne(targetEntity: AdminPayment::class, inversedBy: 'subpayments')]
    private AdminPayment $adminPayment;

    public function getAdminPayment(): AdminPayment
    {
        return $this->adminPayment;
    }

    public function setAdminPayment(AdminPayment $adminPayment): void
    {
        $this->adminPayment = $adminPayment;
    }

    public function getSupplier(): Supplier
    {
        return $this->supplier;
    }

    public function setSupplier(Supplier $supplier): void
    {
        $this->supplier = $supplier;
    }
}
