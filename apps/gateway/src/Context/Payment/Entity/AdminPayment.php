<?php

namespace App\Context\Payment\Entity;

use App\Context\Order\Entity\Order;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;

#[ORM\Table(name: 'admins_payments')]
#[ORM\Entity]
class AdminPayment extends Payment
{
    #[ORM\OneToOne(targetEntity: Order::class, inversedBy: 'payment')]
    #[ORM\JoinColumn(name: 'order_id', referencedColumnName: 'id', nullable: false)]
    private Order $order;

    /** One User has One Catalog. */
    #[OneToMany(targetEntity: SupplierPayment::class, mappedBy: 'adminPayment')]
    private Collection $subpayments;

    public function __construct()
    {
        parent::__construct();
        $this->subpayments = new ArrayCollection();
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function setOrder(Order $order): void
    {
        $this->order = $order;
    }

    public function getSubpayments(): Collection
    {
        return $this->subpayments;
    }

    public function setSubpayments(Collection $subpayments): void
    {
        $this->subpayments = $subpayments;
    }

    public function addSubpayment(SupplierPayment $subpayment)
    {
        if (!$this->subpayments->contains($subpayment)) {
            $this->subpayments->add($subpayment);
            $subpayment->setAdminPayment($this);
        }
    }
}
