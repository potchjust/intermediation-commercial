<?php

namespace App\Context\Payment\UseCase;

use App\Context\Auth\Entity\Customer;
use App\Context\Order\Entity\Order;
use App\Context\Order\Entity\SubOrder;
use App\Context\Order\Enums\OrderStatus;
use App\Context\Order\Repository\OrderRepository;
use App\Context\Payment\Entity\AdminPayment;
use App\Context\Payment\Entity\Payment;
use App\Context\Payment\Entity\SupplierPayment;
use App\Context\Payment\Repository\AdminPaymentRepository;
use App\Context\Payment\Repository\SupplierPaymentRepository;
use App\Context\Whatsapp\Dto\Messages\Text\WhatsappTextMessage;
use App\Context\Whatsapp\Enums\WhatsappMessageType;
use App\Context\Whatsapp\UseCase\Request\SendProcessingMessage;
use App\Context\Whatsapp\UseCase\Request\SendTemplateMessage;
use App\Context\Whatsapp\UseCase\Whatsapp\Messages\SendMessageRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;

final class PayOrder
{
    public function __construct(private readonly SerializerInterface $serializer,
        private readonly OrderRepository $orderRepository,
        private readonly SendProcessingMessage $sendProcessingMessage,
        private readonly SupplierPaymentRepository $supplierPaymentRepository,
        private readonly AdminPaymentRepository $adminPaymentRepository,
        private readonly SendTemplateMessage $sendTemplateMessage,
        private readonly EntityManagerInterface $entityManager)
    {
    }

    public function pay(string $currentOrderReference, Customer $customer)
    {
        $currentOrder = $this->orderRepository->findOneBy([
            'orderReference' => trim($currentOrderReference),
        ]);
        $this->sendProcessingMessage->send($customer);
        if (!$currentOrder) {
            $message = 'La commande avec cette reference n\'existe pas. Veuillez reessayer ';

            return $this->sendOrderNotFoundMessage($customer, $message);
        }

        assert($currentOrder instanceof Order);
        if ($currentOrder->getStatus() !== OrderStatus::HAS_BEEN_VALIDATED_BY_CUSTOMER->name) {
            $message = 'Erreur cette commande ne peux pas etre payee car vous n\' avez pas encore
             valide la commande';

            return $this->sendOrderNotFoundMessage($customer, $message);
        }

        if ($currentOrder->getStatus() === OrderStatus::HAS_BEEN_CANCELLED_BY_CUSTOMER->name) {
            $message = 'Erreur cette commande ne peux pas etre payee car vous l\'avez deja annuler';

            return $this->sendOrderNotFoundMessage($customer, $message);
        }

        if ($currentOrder->getStatus() === OrderStatus::DELIVERED->name) {
            $message = 'Erreur lors du paiement de la commande: Cette commande a déjà été livrée';

            return $this->sendOrderNotFoundMessage($customer, $message);
        }

        if ($currentOrder->getStatus() === OrderStatus::NEW->name || $currentOrder->getStatus() === OrderStatus::PROCESSING->name) {
            $message = 'Cette commande ne peut être payé car elle est en cours de traitement';

            return $this->sendOrderNotFoundMessage($customer, $message);
        }

        if ($currentOrder->getStatus() === OrderStatus::PAID->name) {
            $message = 'Erreur cette commande a deja ete payee';

            return $this->sendOrderNotFoundMessage($customer, $message);
        }
        $currentOrder->setStatus(OrderStatus::PAID->name);

        $adminPayment = new AdminPayment();
        $adminPayment->setReference($this->getAdminReference());
        $adminPayment->setMethod('TEST');
        foreach ($currentOrder->getSubOrders() as $subOrder) {
            assert($subOrder instanceof SubOrder);
            // create the payment order
            $supplierPayment = new SupplierPayment();
            $supplierPayment->setSupplier(supplier: $subOrder->getSupplier());
            $total = 0;
            // get the total
            foreach ($subOrder->getOrderDetails() as $currentOrderDetail) {
                $total = $total + $currentOrderDetail->getPrice();
            }
            $supplierPayment->setAmount($total);
            $supplierPayment->setPaidAt(new \DateTimeImmutable());
            $supplierPayment->setMethod('TEST');
            $supplierPayment->setReference($this->getSupplierReference($subOrder->getSupplier()));
            $adminPayment->addSubpayment($supplierPayment);
            $this->entityManager->persist($supplierPayment);
        }
        $this->entityManager->persist($currentOrder);
        $this->entityManager->persist($adminPayment);
        $this->entityManager->flush();

        return [$this->sendOrderConfirmationMessage($customer)];
    }

    private function sendOrderNotFoundMessage(Customer $customer, string $message): SendMessageRequest
    {
        $textMessage = new WhatsappTextMessage();
        $textMessage->setMessagingProduct('whatsapp');
        $textMessage->setTo($customer->getPhoneNumber());
        $textMessage->setType(WhatsappMessageType::TEXT->value);
        $formattedMessage = $message;
        $textMessage->setContent([
            'preview_url' => false,
            'body' => $formattedMessage,
        ]);
        $request = new SendMessageRequest($this->serializer);
        $request->setBody($textMessage);

        return $request;
    }

    private function getSupplierReference(SupplierPayment $supplierPayment): string
    {
        $supplierPayments = $this->supplierPaymentRepository->getBySupplier(supplier: $supplierPayment->getSupplier());
        $count = count($supplierPayments);
        $nextNumber = $count + 1;

        return sprintf('REF-SUPAY-%03d', $nextNumber);
    }

    private function getAdminReference(): string
    {
        $supplierPayments = $this->adminPaymentRepository->findAll();
        $count = count($supplierPayments);
        $nextNumber = $count + 1;

        return sprintf('REF-PAY-%03d', $nextNumber);
    }

    private function sendOrderConfirmationMessage(Customer $customer): SendMessageRequest
    {
        return $this->sendTemplateMessage->createRequest(
            customer: $customer,
            templateName: 'order_confirmation_message'
        );
    }
}
