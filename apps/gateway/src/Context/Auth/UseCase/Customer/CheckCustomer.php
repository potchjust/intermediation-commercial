<?php

namespace App\Context\Auth\UseCase\Customer;

use App\Context\Auth\Entity\Customer;
use App\Context\Catalog\Repository\CustomerRepository;
use App\Context\Whatsapp\Dto\Webhook\WhatsappWebhookContacts;

final class CheckCustomer
{
    public function __construct(private CustomerRepository $customerRepository, private CreateCustomer $createCustomer)
    {
    }

    public function check(WhatsappWebhookContacts $whatsappWebhookContacts): Customer
    {
        $customer = $this->customerRepository->findOneBy(['phoneNumber' => $whatsappWebhookContacts->getWaId()]);
        if (!$customer) {
            $customer = $this->createCustomer->create($whatsappWebhookContacts);
        }

        return $customer;
    }
}
