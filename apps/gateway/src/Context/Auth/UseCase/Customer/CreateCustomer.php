<?php

namespace App\Context\Auth\UseCase\Customer;

use App\Context\Auth\Entity\Customer;
use App\Context\Whatsapp\Dto\Webhook\WhatsappWebhookContacts;
use Doctrine\ORM\EntityManagerInterface;

final readonly class CreateCustomer
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function create(WhatsappWebhookContacts $whatsappWebhookContacts): Customer
    {
        $newCustomer = new Customer();
        $newCustomer->setPhoneNumber($whatsappWebhookContacts->getWaId());
        $newCustomer->setDisplayedName($whatsappWebhookContacts->getProfileName());
        $this->entityManager->persist($newCustomer);
        $this->entityManager->flush();

        return $newCustomer;
    }
}
