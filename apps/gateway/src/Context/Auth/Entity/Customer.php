<?php

namespace App\Context\Auth\Entity;

use App\Context\Order\Entity\Order;
use App\Context\Whatsapp\Entity\ActionTrails;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;

#[Entity]
#[Table(name: 'customers')]
class Customer extends User
{
    #[Column(length: 180, nullable: false)]
    private string $displayedName;

    /* one user can make one one multiple order */
    #[OneToMany(targetEntity: Order::class, mappedBy: 'customer')]
    private Collection $orders;

    #[Column(length: 180)]
    private string $phoneNumber;

    #[OneToMany(targetEntity: ActionTrails::class, mappedBy: 'customer')]
    private Collection $actionTrails;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->actionTrails = new ArrayCollection();
    }

    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function setOrders(Collection $orders): void
    {
        $this->orders = $orders;
    }

    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getDisplayedName(): string
    {
        return $this->displayedName;
    }

    public function setDisplayedName(string $displayedName): void
    {
        $this->displayedName = $displayedName;
    }

    public function getActionTrails(): Collection
    {
        return $this->actionTrails;
    }

    public function addNewAction(ActionTrails $action): void
    {
        $this->actionTrails->add($action);
        $action->setCustomer($this);
    }
}
