<?php

namespace App\Context\Auth\Entity;

use App\Context\Catalog\Entity\Catalog;
use App\Context\Catalog\Entity\Product;
use App\Context\Order\Entity\SubOrder;
use App\Context\Payment\Entity\SupplierPayment;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Uid\UuidV7;

#[Entity]
#[Table(name: 'suppliers')]
class Supplier extends User
{
    #[Column(length: 180, nullable: true)]
    #[Groups(['supplier:read'])]
    private string $name;

    #[Column(length: 180, nullable: true)]
    #[Groups(['supplier:read'])]
    private string $subdomain;

    #[Column(length: 180, nullable: true)]
    private string $address;

    #[Column(length: 180, nullable: true)]
    private string $phoneNumber;

    /** One User has One Catalog. */
    #[OneToMany(targetEntity: Product::class, mappedBy: 'supplier')]
    private Collection $products;

    #[OneToMany(targetEntity: SubOrder::class, mappedBy: 'supplier')]
    private Collection $subOrders;

    #[OneToMany(targetEntity: SupplierPayment::class, mappedBy: 'supplier')]
    private Collection $payments;

    // vendor
    #[ManyToMany(targetEntity: Supplier::class, inversedBy: 'suppliers')]
    private Collection $wholesalers;

    public function __construct()
    {
        $this->subOrders = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->wholesalers = new ArrayCollection();
    }

    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    public function getSubOrders(): Collection
    {
        return $this->subOrders;
    }

    public function setSubOrders(Collection $subOrders): void
    {
        $this->subOrders = $subOrders;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getSubdomain(): string
    {
        return $this->subdomain;
    }

    public function setSubdomain(string $subdomain): void
    {
        $this->subdomain = $subdomain;
    }

    #[\Override]
    public function getRoles(): array
    {
        return ['ROLE_SUPPLIER'];
    }

    #[\Override]
    public function eraseCredentials(): void
    {
        // TODO: Implement eraseCredentials() method.
    }

    public static function createFromPayload($username, array $payload)
    {
        $supplier = new Supplier();
        $supplier->setId(UuidV7::fromString($payload['id']));
        $supplier->setEmail($payload['email']);
        $supplier->setName($payload['name']);
        $supplier->setSubdomain($payload['subDomain']);

        return $supplier;
    }

    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function setPayments(Collection $payments): void
    {
        $this->payments = $payments;
    }

    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addNewProduct(Product $product): void
    {
        if (!$this->products->contains($product)) {
            $this->products->add($product);
            $product->setSupplier($this);
        }
    }

    public function setProducts(Collection $products): void
    {
        $this->products = $products;
    }
}
