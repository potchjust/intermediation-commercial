<?php

namespace App\Context\Auth\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;

#[Entity]
#[Table(name: 'admins')]
class Admin extends User
{
    #[\Override]
    public function getRoles(): array
    {
        return ['ROLE_ADMIN'];
    }

    #[\Override]
    public function eraseCredentials(): void
    {
        // TODO: Implement eraseCredentials() method.
    }
}
