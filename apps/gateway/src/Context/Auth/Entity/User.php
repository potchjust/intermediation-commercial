<?php

namespace App\Context\Auth\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Context\Auth\Repository\UsersRepository;
use App\Context\Catalog\Entity\Wholesaler;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Uid\UuidV7;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UsersRepository::class)]
#[ORM\Table(name: 'users')]
#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string')]
#[ORM\DiscriminatorMap([
    'customer' => Customer::class,
    'supplier' => Supplier::class,
    'admin' => Admin::class,
    'wholesaler' => Wholesaler::class,
])]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator('doctrine.uuid_generator')]
    private UuidV7 $id;

    public function getId(): UuidV7
    {
        return $this->id;
    }

    public function setId(UuidV7 $id): void
    {
        $this->id = $id;
    }

    #[Column(type: 'string', length: 255, nullable: true)]
    #[Assert\Blank]
    #[Assert\Length(min: 5, max: 100)]
    #[Assert\Email]
    #[Groups(['supplier:read'])]
    private string $email;

    #[Column(length: 180, nullable: true)]
    private string $password;

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getRoles(): array
    {
        return [];
    }

    public function eraseCredentials(): void
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getUserIdentifier(): string
    {
        return $this->email;
    }
}
