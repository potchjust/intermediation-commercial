<?php

namespace App\Context\Catalog\Repository;

use App\Context\Catalog\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function getProducyById(string $id): Product
    {
        return $this->findOneBy(['id' => $id]);
    }

    public function getProducyByWoocommerceId(string $id): ?Product
    {
        return $this->findOneBy(['woocommerce_id' => $id]);
    }

    public function getOrderProductsAlternatives(array $productIds)
    {
        return $this->createQueryBuilder('p')
            ->where('p.id NOT IN (:productsId)')
            ->setParameter('productsId', $productIds)
            ->orderBy('p.label', 'ASC')->getQuery()->getResult();
    }
}
