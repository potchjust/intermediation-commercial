<?php

namespace App\Context\Catalog\Repository;

use App\Context\Auth\Entity\Supplier;
use App\Context\Catalog\Entity\Catalog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CatalogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Catalog::class);
    }

    public function findAllBySupplier(Supplier $supplier): array
    {
        $qb = $this->createQueryBuilder('catalogs')
            ->where('catalogs.supplier = :supplierId')
            ->setParameter('supplierId', $supplier->getId());
        $query = $qb->getQuery();

        return $query->execute();
    }
}
