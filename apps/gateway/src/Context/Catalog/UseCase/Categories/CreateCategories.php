<?php

namespace App\Context\Catalog\UseCase\Categories;

use App\Context\Catalog\Entity\Category;
use App\Infrastructure\Wordpress\Woocommerce\Request\WoocommerceSendPostRequest;
use App\Infrastructure\Wordpress\Woocommerce\WooCommerceApi;
use Doctrine\ORM\EntityManagerInterface;

readonly class CreateCategories
{
    public function __construct(private EntityManagerInterface $entityManager, private WooCommerceApi $wooCommerceApi)
    {
    }

    public function create(string $categoryName, ?string $description = null): void
    {
        $category = new Category();
        $category->setLabel($categoryName);
        $category->setDescription($description);
        $this->createCategoriesOnWooCommerce($category);
        $this->entityManager->persist($category);
        $this->entityManager->flush();
    }

    public function createCategoriesOnWooCommerce(Category $category): void
    {
        $createCategoriesRequest = new WoocommerceSendPostRequest('products/categories');
        $productCategory = [];
        $productCategory['slug'] = $category->getId()->toRfc4122();
        $productCategory['name'] = $category->getLabel();
        $createCategoriesRequest->addParameter($productCategory);
        $response = json_decode($this->wooCommerceApi->send($createCategoriesRequest), true);
        if (null !== $response['id']) {
            $category->setWoocommerceId($response['id']);
        }
    }
}
