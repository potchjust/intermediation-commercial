<?php

namespace App\Context\Catalog\UseCase\Categories;

use App\Context\Catalog\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;

readonly class DeleteCategories
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function delete(Category $category): void
    {
        $this->entityManager->detach($category);
        $this->entityManager->flush();
    }
}
