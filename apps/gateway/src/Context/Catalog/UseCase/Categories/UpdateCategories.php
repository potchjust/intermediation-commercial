<?php

namespace App\Context\Catalog\UseCase\Categories;

use App\Context\Catalog\Entity\Category;
use App\Infrastructure\Wordpress\Woocommerce\Request\WoocommerceSendPostRequest;
use Doctrine\ORM\EntityManagerInterface;

readonly class UpdateCategories
{
    public function __construct(private EntityManagerInterface $entityManager, private WoocommerceSendPostRequest $woocommerceSendPostRequest)
    {
    }

    public function update(Category $category, string $name, ?string $description = null): void
    {
        $category->setLabel($name);
        $category->setDescription($description);
        $this->entityManager->persist($category);
        $this->entityManager->flush();
    }
}
