<?php

namespace App\Context\Catalog\UseCase;

use App\Context\Catalog\Entity\Catalog;
use Doctrine\ORM\EntityManagerInterface;

class CreateCatalog
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function create(string $catalogName): void
    {
        $catalog = new Catalog();
        $catalog->setLabel($catalogName);
        $this->entityManager->persist($catalog);
        $this->entityManager->flush();
    }
}
