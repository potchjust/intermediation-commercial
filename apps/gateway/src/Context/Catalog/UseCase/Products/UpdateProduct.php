<?php

namespace App\Context\Catalog\UseCase\Products;

use App\Context\Catalog\Entity\Category;
use App\Context\Catalog\Entity\Product;
use App\Infrastructure\Wordpress\Woocommerce\Request\WoocommerceSendUpdateRequest;
use App\Infrastructure\Wordpress\Woocommerce\WooCommerceApi;
use Doctrine\ORM\EntityManagerInterface;

readonly class UpdateProduct
{
    public function __construct(private readonly EntityManagerInterface $entityManager, private readonly WooCommerceApi $wooCommerceApi)
    {
    }

    public function execute(Product $product): void
    {
        $this->editOnWoocommerce($product);
        $this->entityManager->persist($product);
        $this->entityManager->flush();
    }

    private function editOnWoocommerce(Product $product): void
    {
        $createProductsRequest = new WoocommerceSendUpdateRequest("products/{$product->getWoocommerceId()}");
        $requestItem['name'] = $product->getLabel();
        $requestItem['description'] = $product->getDescription();
        $requestItem['regular_price'] = strval($product->getPrice());
        $requestItem['type'] = 'simple';
        $requestItem['sku'] = $product->getId()->toRfc4122();
        $categories = [];

        foreach ($product->getCategories() as $category) {
            assert($category instanceof Category);
            if (null == $category->getWoocommerceId()) {
                continue;
            }
            // faire une recherche par nom
            $categories[] = [
                'id' => $category->getWoocommerceId(),
            ];
        }
        $requestItem['categories'] = $categories;
        $createProductsRequest->addParameter($requestItem);
        $response = json_decode($this->wooCommerceApi->send($createProductsRequest), true);
        if ($response['id']) {
            $product->setWoocommerceId($response['id']);
        }
        $createProductsRequest->flushParameters();
    }
}
