<?php

namespace App\Context\Catalog\Form;

use App\Context\Catalog\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class OrderDetailsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('product', EntityType::class, [
                'class' => Product::class,
                'choice_label' => 'label',
            ])
            ->add('productName', TextType::class)
            ->add('quantity', IntegerType::class)
            ->add('status', TextType::class)
            ->add('unitPrice', IntegerType::class)
            ->add('subTotal', IntegerType::class)
            ->add('isInStock', TextType::class)
            ->add('validate', SubmitType::class, [
                'label' => 'validate',
            ])
            ->add('cancel', SubmitType::class, [
                'label' => 'cancel',
            ])

        /*     $this->id = $orderDetails->getId();
        $this->quantity = $orderDetails->getQuantity();
        $this->status = $orderDetails->getStatus();
        $this->productName = $orderDetails->getProduct()->getLabel();
        $this->unitPrice = $orderDetails->getProduct()->getPrice();
        $this->subTotal = $orderDetails->getQuantity() * $orderDetails->getProduct()->getPrice();*/
        ;
    }
}
