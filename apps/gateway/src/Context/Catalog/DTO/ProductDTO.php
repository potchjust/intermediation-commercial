<?php

namespace App\Context\Catalog\DTO;

use App\Context\Catalog\Entity\Category;
use App\Context\Catalog\Entity\Product;
use Symfony\Component\Uid\UuidV7;

final class ProductDTO
{
    private UuidV7 $id;
    private string $label = '';
    private string $imageUrl = '';
    private string $categories = '';
    private string $orderCount = '';
    private string $price = '';
    private string $inStock = 'Yes';

    public function __construct(Product $product)
    {
        $this->id = $product->getId();
        $this->label = $product->getLabel();
        $this->imageUrl = $product->getImage() ?? '';
        foreach ($product->getCategories() as $category) {
            assert($category instanceof Category);
            $this->categories = $this->categories.$category->getLabel().' | ';
        }
        $this->orderCount = 0;
        $this->price = $product->getPrice().' FCFA';
        $this->inStock = $product->getCount() > 0 ? 'Yes' : 'No'; // use false
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function getImageUrl(): string
    {
        return $this->imageUrl;
    }

    public function setImageUrl(string $imageUrl): void
    {
        $this->imageUrl = $imageUrl;
    }

    public function getCategories(): string
    {
        return $this->categories;
    }

    public function setCategories(string $categories): void
    {
        $this->categories = $categories;
    }

    public function getOrderCount(): string
    {
        return $this->orderCount;
    }

    public function setOrderCount(string $orderCount): void
    {
        $this->orderCount = $orderCount;
    }

    public function getPrice(): string
    {
        return $this->price;
    }

    public function setPrice(string $price): void
    {
        $this->price = $price;
    }

    public function getInStock(): string
    {
        return $this->inStock;
    }

    public function setInStock(string $inStock): void
    {
        $this->inStock = $inStock;
    }

    public function getId(): UuidV7
    {
        return $this->id;
    }

    public function setId(UuidV7 $id): void
    {
        $this->id = $id;
    }
}
