<?php

namespace App\Context\Catalog\DTO;

class CategoryDTO
{
    private string $label;
    private string $description;
    private ?string $catalog = null;
    private int $productCount;

    public function __construct(string $label, string $description, int $productCount, ?string $catalog = null)
    {
        $this->label = $label;
        $this->description = $description;
        $this->catalog = $catalog;
        $this->productCount = $productCount;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getCatalog(): ?string
    {
        return $this->catalog;
    }

    public function setCatalog(?string $catalog): void
    {
        $this->catalog = $catalog;
    }

    public function getProductCount(): int
    {
        return $this->productCount;
    }

    public function setProductCount(int $productCount): void
    {
        $this->productCount = $productCount;
    }
}
