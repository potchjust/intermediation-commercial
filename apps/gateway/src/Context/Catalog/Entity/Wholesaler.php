<?php

namespace App\Context\Catalog\Entity;

use App\Context\Auth\Entity\Supplier;
use App\Context\Auth\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Serializer\Attribute\Groups;

#[Entity]
#[Table(name: 'wholesalers')]
class Wholesaler extends User
{
    #[Column(length: 180, nullable: true)]
    #[Groups(['supplier:read'])]
    private string $name;

    #[Column(length: 180, nullable: true)]
    #[Groups(['supplier:read'])]
    private string $subdomain;

    #[Column(length: 180, nullable: true)]
    private string $address;

    #[Column(length: 180, nullable: true)]
    private string $phoneNumber;

    #[Column(length: 180, nullable: true)]
    private string $description;

    #[ORM\ManyToMany(targetEntity: Supplier::class, mappedBy: 'wholesalers')]
    public ArrayCollection $suppliers;

    public function __construct()
    {
        $this->suppliers = new ArrayCollection();
    }
}
