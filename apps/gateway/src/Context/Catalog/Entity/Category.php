<?php

namespace App\Context\Catalog\Entity;

use App\Context\Catalog\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\ManyToMany;
use Symfony\Component\Uid\UuidV7;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
#[ORM\Table(name: 'categories')]
class Category
{
    #[ORM\Id]
    #[Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator('doctrine.uuid_generator')]
    private UuidV7 $id;

    #[Column(length: 255, nullable: true)]
    private ?string $label = null;

    #[Column(type: 'text', length: 255, nullable: true)]
    private ?string $description = null;

    #[ManyToMany(targetEntity: Product::class, mappedBy: 'categories')]
    private Collection $products;

    #[Column(type: 'integer', nullable: true)]
    private ?int $woocommerce_id = null;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): void
    {
        $this->label = $label;
    }

    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function getId(): UuidV7
    {
        return $this->id;
    }

    public function setId(UuidV7 $id): void
    {
        $this->id = $id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getWoocommerceId(): ?int
    {
        return $this->woocommerce_id;
    }

    public function setWoocommerceId(?int $woocommerce_id): void
    {
        $this->woocommerce_id = $woocommerce_id;
    }

}
