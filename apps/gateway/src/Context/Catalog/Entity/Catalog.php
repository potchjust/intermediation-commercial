<?php

namespace App\Context\Catalog\Entity;

use App\Context\Auth\Entity\Supplier;
use App\Context\Catalog\Repository\CatalogRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Uid\UuidV7;

#[ORM\Entity(repositoryClass: CatalogRepository::class)]
#[ORM\Table(name: 'catalogs')]
class Catalog
{
    #[ORM\Id]
    #[Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator('doctrine.uuid_generator')]
    private UuidV7 $id;

    #[Column(length: 255, nullable: true)]
    private string $label;

    #[ORM\OneToOne(targetEntity: Supplier::class, inversedBy: 'catalog')]
    #[JoinColumn(name: 'supplier_id', referencedColumnName: 'id')]
    private Supplier $supplier;

    #[ORM\OneToMany(targetEntity: Product::class, mappedBy: 'catalog')]
    private Collection $products;

    #[ORM\OneToMany(targetEntity: Category::class, mappedBy: 'catalog')]
    private Collection $categories;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): void
    {
        if (!$this->products->contains($product)) {
            $this->products->add($product);
            $product->setCatalog($this);
        }
    }

    public function removeProduct(Product $product): void
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            if (null !== $product->getCatalog()) {
                $product->setCatalog(null);
            }
        }
    }

    public function setProducts(Collection $products): void
    {
        $this->products = $products;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function getSupplier(): Supplier
    {
        return $this->supplier;
    }

    public function setSupplier(Supplier $supplier): void
    {
        $this->supplier = $supplier;
    }

    public function getId(): UuidV7
    {
        return $this->id;
    }

    public function setId(UuidV7 $id): void
    {
        $this->id = $id;
    }

    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function setCategories(Collection $categories): void
    {
        $this->categories = $categories;
    }
}
