<?php

namespace App\Context\Catalog\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Context\Auth\Entity\Supplier;
use App\Context\Catalog\Repository\ProductRepository;
use App\Context\Order\Entity\OrderDetails;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\CustomIdGenerator;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Uid\UuidV7;

#[Entity(repositoryClass: ProductRepository::class)]
#[Table(name: 'products')]
#[ApiResource]
class Product
{
    #[Id]
    #[Column(type: 'uuid', unique: true)]
    #[GeneratedValue(strategy: 'CUSTOM')]
    #[CustomIdGenerator('doctrine.uuid_generator')]
    private UuidV7 $id;

    #[Column(length: 255, nullable: true)]
    private ?string $label = null;

    #[Column(length: 255, nullable: true)]
    private ?string $description = null;

    #[Column(length: 255, nullable: true)]
    private ?string $image = null;

    #[Column(length: 255, nullable: true)]
    private ?string $slug = null;

    #[ManyToOne(targetEntity: Supplier::class, inversedBy: 'products')]
    private Supplier $supplier;
    // relation entre les produits et les categories
    #[ManyToMany(targetEntity: Category::class, inversedBy: 'products')]
    private Collection $categories;

    #[OneToMany(targetEntity: OrderDetails::class, mappedBy: 'product')]
    private Collection $orderDetails;

    #[Column(type: 'float', nullable: false)]
    private float $price;

    #[Column(type: 'integer', nullable: true)]
    private ?int $woocommerce_id;

    #[Column(type: 'integer', nullable: true)]
    private ?int $count;

    public function __construct()
    {
        $this->id = UuidV7::v7();
        $this->categories = new ArrayCollection();
        $this->orderDetails = new ArrayCollection();
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): void
    {
        $this->label = $label;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): void
    {
        $this->image = $image;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): void
    {
        $this->slug = $slug;
    }

    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function setCategories(Collection $categories): void
    {
        $this->categories = $categories;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    public function addCategory(Category $category): void
    {
        if (!$this->categories->contains($category)) {
            $this->categories->add($category);
        }
    }

    public function addOrderDetail(OrderDetails $orderDetails): void
    {
        if (!$this->orderDetails->contains($orderDetails)) {
            $this->orderDetails->add($orderDetails);
        }
    }

    public function getId(): UuidV7
    {
        return $this->id;
    }

    public function setId(UuidV7 $id): void
    {
        $this->id = $id;
    }

    public function getWoocommerceId(): ?int
    {
        return $this->woocommerce_id;
    }

    public function setWoocommerceId(?int $woocommerce_id): void
    {
        $this->woocommerce_id = $woocommerce_id;
    }

    public function getSupplier(): Supplier
    {
        return $this->supplier;
    }

    public function setSupplier(Supplier $supplier): void
    {
        $this->supplier = $supplier;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(?int $count): void
    {
        $this->count = $count;
    }
}
